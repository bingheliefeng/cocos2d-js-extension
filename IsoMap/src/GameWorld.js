/**
 * Created by zhouzhanglin on 15/8/18.
 */
var GameWorld = BaseWorld.extend({
    groundScene:null,
    buildingScene:null,
    onEnter:function(){
        this._super();

        this.groundScene = new iso.Scene(GRID_SIZE,GRID_X,GRID_Z);
        this.buildingScene = new iso.Scene(GRID_SIZE,GRID_X,GRID_Z);

        this.addScene(this.groundScene);
        this.addScene(this.buildingScene);
    },
    /**
     * 重写父类方法
     * @param gridPos 网格坐标
     */
    onClick:function(gridPos){
        if(this._gridData.checkInGrid(gridPos.x,gridPos.y)){
            if(this._gridData.getNode(gridPos.x,gridPos.y).walkable){
                var building = this.createCocObject();
                building.setNodeX(gridPos.x);
                building.setNodeZ(gridPos.y);
                if(building.getWalkable(this._gridData)){
                    building.setWalkable(false,this._gridData);
                    this.buildingScene.addIsoObject(building,true);
                }
                else
                {
                    building.dispose();
                    building = null;
                }
            }
        }
    },
    createCocObject:function(){
        var temp = Math.random()>0.5 ? 1 : 2;
        var building = new Building(temp,temp);
        return building;
    }

});