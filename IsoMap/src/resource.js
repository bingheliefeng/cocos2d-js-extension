var res = {
    HelloWorld_png : "res/HelloWorld.png",
    CloseNormal_png : "res/CloseNormal.png",
    CloseSelected_png : "res/CloseSelected.png",
    Background_png:"res/isoRes/Background.png",
    Stove_png:"res/isoRes/stove.png",
    Host_png:"res/isoRes/host.png",
    BuildingOK_png:"res/building_ok.png",
    BuildingCancel_png:"res/building_cancel.png",
    ShopBtn_png:"res/frame_add.png",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}