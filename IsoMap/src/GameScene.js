/**
 * Created by zhouzhanglin on 15/8/19.
 */
var GameScene = cc.Scene.extend({
    gameWorld:null,
    uiLayer:null,

    ctor:function(){
        this._super();
        GameScene.instance = this;
    },
    onEnter:function(){
        this._super();
        this.gameWorld=new GameWorld();
        this.addChild(this.gameWorld);

        this.uiLayer = new GameUILayer();
        this.addChild(this.uiLayer);

    }

});