/**
 * Created by zhouzhanglin on 15/8/18.
 */
GRID_SIZE = 40; //size大小
GRID_X = 15 ; //x轴的网格大小
GRID_Z = 15;  // z轴的网格大小
MAP_WIDTH = 1024*2;
MAP_HEIGHT = 768*2;

var BaseWorld = iso.World.extend({
    _moveSpeed:0,
    _isClickNode:false,
    _touches:null,
    _scaleMatrix:null,
    _gridData:null,
    _endX:0,
    _endY:0,
    _beganWorldX:0,
    _beganWorldY:0,

    ctor:function(){
        this._super(GRID_SIZE,GRID_X,GRID_Z);

        this._touches = [];
        this._scaleMatrix = new iso.Matrix();

        this._gridData = new iso.PathGrid(GRID_X,GRID_Z);
        this._gridData.setAllWalkable(true);

        this.setPosition(cc.winSize.width/2-MAP_WIDTH/2,cc.winSize.height+150);
        this.panSceneLayerTo(MAP_WIDTH/2-20,-320 );

        this._endX = this.getPositionX();
        this._endY = this.getPositionY();
    },
    onEnter:function(){
        this._super();

        //添加背景图片
        var bg = new cc.Sprite(res.Background_png);
        bg.setAnchorPoint(0,1);
        bg.setScale(2,2);
        this.setBg(bg);

        //添加网格.
        var gridNode = new iso.Grid(GRID_SIZE,GRID_X,GRID_Z,100);
        this.addScene(gridNode);
        gridNode.render();

        //添加鼠标事件
        var listeners = cc.EventListener.create({
            event:cc.EventListener.TOUCH_ALL_AT_ONCE,
            //swallowTouches:false,
            onTouchesBegan:this.onTouchesBegan.bind(this),
            onTouchesMoved:this.onTouchesMoved.bind(this),
            onTouchesEnded:this.onTouchesEnded.bind(this),
            onTouchesCancelled:this.onTouchesEnded.bind(this)
        });
        cc.eventManager.addListener(listeners,this);
        //每帧执行.
        this.scheduleUpdate();
    },
    onTouchesBegan:function(touches,event){
        var len = touches.length;
        for(var i=0;i<len;i++)
        {
            var touch = touches[i];
            var id = touch.getID();
            var point = touch.getLocation();
            this._touches.push( {
                beganX:point.x,
                beganY:point.y,
                currentX:point.x,
                currentY:point.y,
                prevX:point.x,
                prevY:point.y,
                type:"began" ,
                id : id
            });
        }
        if(touches.length==1){
            this._moveSpeed=40;
            this._beganWorldX = this.getPositionX();
            this._beganWorldY = this.getPositionY();
            this._endX = this._beganWorldX;
            this._endY = this._beganWorldY;
        }
        return true;
    },
    onTouchesMoved:function(touches,event){
        var len = touches.length;
        for(var i=0;i<len;i++)
        {
            var touch = touches[i];
            var id = touch.getID();
            var point = touch.getLocation();
            var prevPoint = touch.getPreviousLocation();

            for(var j=0;j<this._touches.length;j++){
                var v = this._touches[j];
                if(v.id==id){
                    v.currentX = point.x;
                    v.currentY = point.y;
                    v.prevX = prevPoint.x;
                    v.prevY = prevPoint.y;
                    v.type = "moved";
                }
            }
        }
        if(this._touches.length==1){
            var deltaX = this._touches[0].currentX-this._touches[0].beganX;
            var deltaY = this._touches[0].currentY-this._touches[0].beganY;
            this._endX = this._beganWorldX+deltaX;
            this._endY = this._beganWorldY+deltaY;
            this.modifyEndPosition();
        }else if(this._touches.length>1){

            //中点
            var middleX = (this._touches[0].beganX+this._touches[1].beganX)/2;
            var middleY = (this._touches[0].beganY+this._touches[1].beganY)/2;

            var currentPosA  = cc.p( this._touches[0].currentX , this._touches[0].currentY );
            var previousPosA = cc.p( this._touches[0].prevX , this._touches[0].prevY );
            var currentPosB  = cc.p( this._touches[1].currentX , this._touches[1].currentY );
            var previousPosB = cc.p( this._touches[1].prevX , this._touches[1].prevY );

            var currentVector = cc.pSub(currentPosA,currentPosB );
            var previousVector = cc.pSub(previousPosA,previousPosB);

            //scale
            var sizeDiff = cc.pLength(currentVector) / cc.pLength(previousVector);
            if (cc.winSize.height>640) {
                sizeDiff = sizeDiff > 1 ?  1 + (sizeDiff - 1) * 1.4 : 1 - (1 - sizeDiff) * 1.4
            }

            if(sizeDiff<1){
                if(this.getScaleX()>1){
                    this.scaleWorld(sizeDiff,middleX,middleY);
                }
            }else if(sizeDiff>1){
                if(this.getScaleY()<2.5){
                    this.scaleWorld(sizeDiff,middleX,middleY);
                }
            }
        }
    },
    onTouchesEnded:function(touches,event,type){
        if(touches.length==0) return;
        var len = touches.length;
        for(var i=0;i<len;i++){
            var touch = touches[i];
            var id = touch.getID();
            var point = touch.getLocation();
            for(var j=0;j<this._touches.length;j++){
                var v = this._touches[j];
                if(v.id==id){
                    v.currentX = point.x;
                    v.currentY = point.y;
                    v.type = type;
                }
            }
        }

        if(this._touches.length==1){
            var dis = cc.pDistance(cc.p(this._touches[0].currentX,this._touches[0].currentY),cc.p(this._touches[0].beganX,this._touches[0].beganY));
            if(dis>6){
                this._moveSpeed = 5;
                this._endX = this._endX + (this._touches[0].currentX-this._touches[0].prevX)*6/this.getScaleX();
                this._endY = this._endY + (this._touches[0].currentY-this._touches[0].prevY)*6/this.getScaleY();
                this.modifyEndPosition();
            }else if(dis<6){
                //点击
                var pos = this.pixelPointToGrid(this._touches[0].currentX,this._touches[0].currentY);
                this.onClick(pos);
            }
        }

        this._touches=[];
    },
    modifyEndPosition:function(){
        if (this._endY<cc.winSize.height) this._endY=cc.winSize.height;
        else if(this._endY>MAP_HEIGHT*this.getScaleY()) this._endY = MAP_HEIGHT*this.getScaleY();

        if (this._endX>0) this._endX=0;
        else if(this._endX<-MAP_WIDTH*this.getScaleX()+cc.winSize.width)
            this._endX = -MAP_WIDTH*this.getScaleX()+cc.winSize.width;
    },
    update:function(dt){
        var speed = dt*this._moveSpeed;
        this.setPosition(
            this.getPositionX()+(this._endX-this.getPositionX())*speed,
            this.getPositionY()+(this._endY-this.getPositionY())*speed
        );
    },
    scaleWorld:function( sizeDiff,middleX,middleY ){
        this._scaleMatrix.identity();
        this._scaleMatrix.scale(this.getScaleX(),this.getScaleY());
        this._scaleMatrix.translate(this.getPositionX(),this.getPositionY());
        this._scaleMatrix.tx = this._scaleMatrix.tx - middleX;
        this._scaleMatrix.ty = this._scaleMatrix.ty - middleY;

        this._scaleMatrix.scale(sizeDiff,sizeDiff);

        this._scaleMatrix.tx = this._scaleMatrix.tx + middleX;
        this._scaleMatrix.ty = this._scaleMatrix.ty + middleY;

        this.setScaleX(this._scaleMatrix.a);
        this.setScaleY(this._scaleMatrix.d);

        this.setPosition(this._scaleMatrix.tx,this._scaleMatrix.ty);

        this._endX = this._scaleMatrix.tx;
        this._endY = this._scaleMatrix.ty;

        if(this._endY<cc.winSize.height){
            this._endY=cc.winSize.height;
            this.setPositionY(cc.winSize.height );
        }else if(this._endY > MAP_HEIGHT*this.getScaleY()){
            this._endY = MAP_HEIGHT*this.getScaleY();
            this.setPositionY(this._endY);
        }

        if(this._endX>0){
            this._endX=0;
            this.setPositionX(0);
        }else if(this._endX<-MAP_WIDTH*this.getScaleX()+cc.winSize.width){
            this._endX = -MAP_WIDTH*this.getScaleX()+cc.winSize.width;
            this.setPositionX(this._endX);
        }
    },
    onClick:function(gridPos){

    }
});