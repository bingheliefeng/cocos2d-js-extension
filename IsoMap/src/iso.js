/**
 * Created by zhouzhanglin on 15/8/17.
 */
var iso = iso || {}

iso.Y_CORRECT = Math.cos(-Math.PI / 6) * Math.sqrt(2);
iso.W_H_RATE = 0.75; //宽高比,0.75为coc地图，格式宽为40，高为30。0.5为正常的45度地图，格式宽为60，高为30


//======================================================
//======================================================
/**
 * iso工具类
 */
iso.util = {}

/**
 * iso坐标转到场景坐标
 * @param px
 * @param py
 * @param pz
 * @returns {{x, y}|cc.Point}
 */
iso.util.isoToScreen = function(px,py,pz) {
    var screenX = px - pz;
    var screenY = py * iso.Y_CORRECT + (px + pz) * iso.W_H_RATE;
    return cc.p(screenX, -screenY);
}

/**
 * 场景坐标到iso地图像素坐标的转换
 * @param px
 * @param py
 * @returns {{x, y}|cc.Point}
 */
iso.util.screenToIso = function(px,py){
    var zpos=(-py-px*iso.W_H_RATE)/(2*iso.W_H_RATE);
    var xpos = px+zpos;
    var ypos = 0;
    return cc.p(xpos,ypos);
}

/**
 * 场景坐标转到iso网格坐标
 * @param size
 * @param px
 * @param py
 * @returns {{x, y}|cc.Point}
 */
iso.util.screenToIsoGrid = function(size,px,py){
    var zpos = ( -py-px*iso.W_H_RATE )/(2*iso.W_H_RATE);
    var xpos = px + zpos;

    var col = Math.floor ( xpos / size );
    var row = Math.floor ( zpos / size );
    return cc.p(col,row);
}

//======================================================
//======================================================

/**
 * 矩阵
 * @type {Function}
 */
iso.Matrix = cc.Class.extend({
    a:1,b:0,c:0,d:1,tx:0,ty:0,
    ctor:function( a,b,c,d,tx,ty)
    {
        this.a = a || 1;
        this.b = b || 0;
        this.c = c || 0;
        this.d = d || 1;
        this.tx = tx || 0;
        this.ty = ty || 0;
    },
    rotate:function(angle)
    {
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        var a1 = this.a;
        var c1 = this.c;
        var tx1 = this.tx;

        this.a = a1 * cos - this.b * sin;
        this.b = a1 * sin + this.b * cos;
        this.c = c1 * cos - this.d * sin;
        this.d = c1 * sin + this.d * cos;
        this.tx = tx1 * cos - this.ty * sin;
        this.ty = tx1 * sin + this.ty * cos;
        return this;
    },
    scale:function(x,y){
        this.a = this.a*x;
        this.d =this.d * y;
        this.tx = this.tx* x;
        this.ty = this.ty * y;
        return this;
    },
    translate:function(tx,ty){
        this.tx = this.tx+tx;
        this.ty = this.ty + ty;
        return this;
    },
    identity:function(){
        this.a = 1;
        this.d = 1;
        this.b = 0;
        this.c = 0;
        this.tx = 0;
        this.ty = 0;
        return this;
    },
    transformPoint:function(pt){
        return cc.p(
            this.a * pt.x + this.c * pt.y + this.tx,
            this.d * pt.y + this.b * pt.x + this.ty
        )
    },
    deltaTransformPoint:function(pt){
        return cc.p(
            this.a * pt.x + this.c * pt.y ,
            this.d * pt.y + this.b * pt.x
        )
    },
    createBox:function(scaleX, scaleY, rotation, tx, ty){
        var u = Math.cos(rotation);
        var v = Math.sin(rotation);
        this.a = u * scaleX;
        this.b = v * scaleY;
        this.c = -v * scaleX;
        this.d = u * scaleY;
        this.tx = tx;
        this.ty = ty;
        return this;
    },
    createGradientBox:function(width, height, rotation, tx, ty){
        return this.createBox(
            width / 1638.4,
            height / 1638.4,
            rotation,
            tx + width / 2,
            ty + height / 2
        );
    },
    invert:function(){
        var a = this.a;
        var b = this.b;
        var c = this.c;
        var d = this.d;
        var tx = this.tx;
        var ty = this.ty;
        var det = a * d - b * c;

        if (!det) {
            this.identity();
            return;
        }

        det = 1 / det;
        this.a = d * det;
        this.b = -b * det;
        this.c = -c * det;
        this.d = a * det;
        this.tx = -(a * tx + c * ty);
        this.ty = -(b * tx + d * ty);
        return this;
    },
    concat: function (m) {
        var a1 = this.a;
        var a2 = m.a;
        var b1 = this.b;
        var b2 = m.b;
        var c1 = this.c;
        var c2 = m.c;
        var d1 = this.d;
        var d2 = m.d;
        var tx1 = this.tx;
        var tx2 = m.tx;
        var ty1 = this.ty;
        var ty2 = m.ty;
        this.a = a1 * a2 + b1 * c2;
        this.b = a1 * b2 + b1 * d2;
        this.c = c1 * a2 + d1 * c2;
        this.d = d1 * d2 + c1 * b2;
        this.tx = (tx1 * a2 + tx2) + ty1 * c2;
        this.ty = (ty1 * d2 + ty2) + tx1 * b2;
        return this;
    }
});



//======================================================
//======================================================

/**
 * 用于寻路的节点
 */
iso.PathNode = cc.Class.extend({
    x:0,y:0,f:0,g:0,h:0,
    walkable:false,
    parent:null,
    links:null,
    version:1,
    ctor:function(nodeX,nodeY){
        this.x = nodeX;
        this.y = nodeY;
    },
    clone:function()
    {
       return new iso.PathNode(this.x,this.y);
    }
});

iso.PathLink = cc.Class.extend({
    node :null,
    cost :0,
    ctor:function(pathNode,cost)
    {
        this.node=pathNode;
        this.cost = cost;
    }
});

/**
 * 节点网格
 * @type {Function}
 */
iso.PathGrid = cc.Class.extend({
    _gridX:0,_gridZ:0,
    _startNode:null,
    _endNode:null,
    _nodes:null,
    _type:0,
    _straightCost:1.0,
    _diagCost:Math.sqrt(2),
    ctor:function(gridX,gridZ){
        this._gridX = gridX;
        this._gridZ = gridZ;
        this._nodes=[];
        for(var i=0;i<gridX;i++){
            this._nodes[i]=[];
            for(var j=0;j<gridZ;j++){
                this._nodes[i][j]=new iso.PathNode(i,j);
            }
        }
    },
    setAllWalkable:function(value){
        for(var i=0;i<this._gridX;i++){
            for(var j=0;j<this._gridZ;j++){
                this._nodes[i][j].walkable = value;
            }
        }
    },
    changeSize:function(gridX , gridZ){
        if(this._gridX!=gridX || this._gridZ!=gridZ){
            var nodes=[];
            for(var i=0;i<gridX;i++){
                 nodes[i]=[];
                for(var j=0;j<gridZ;j++){
                    if (i<this._gridX && j<this._gridZ)
                        nodes[i][j] = self._nodes[i][j];
                    else
                        nodes[i][j] =new iso.PathNode(i,j);
                }
            }
            this._gridX = gridX;
            this._gridZ = gridZ;
            this._nodes = nodes;
        }
    },
    getNodesByWalkable:function(walkable){
        var nodes=[];
        for(var i=0;i<this._gridX;i++){
            for(var j=0;j<this._gridZ;j++){
                var node = this.getNode(i,j);
                if(node.walkable==walkable) nodes.push(node);
            }
        }
        return nodes;
    },
    initNodeLink:function(node,type){
        var startX = Math.max(0, node.x - 1);
        var endX = Math.min(this._gridZ - 1, node.x + 1);
        var startY = Math.max(0, node.y - 1);
        var endY = Math.min(this._gridX - 1, node.y + 1);
        node.links = [];
        for (var i = startX; i <= endX; i++){
            for (var j = startY; j <= endY; j++){
                var test = this.getNode(i, j);
                if (test == node || !test.walkable){
                    continue;
                }
                if (type != 2 && i != node.x && j != node.y){
                    var test2 = this.getNode(node.x, j);
                    if (!test2.walkable){
                        continue;
                    }
                    test2 = this.getNode(i, node.y);
                    if (!test2.walkable){
                        continue;
                    }
                }
                var cost = this._straightCost;
                if (!((node.x == test.x) || (node.y == test.y))){
                    if (type == 1){
                        continue;
                    }
                    if (type == 2 && (node.x - test.x) * (node.y - test.y) == 1){
                        continue;
                    }
                    if (type == 2){
                        cost = this._straightCost;
                    } else {
                        cost = this._diagCost;
                    }
                }
                node.links.push(new iso.PathLink(test, cost));
            }
        }
    },
    checkInGrid:function(x,y){
        if(x<0 || y<0 || x>=this._gridX || y>=this._gridZ) return false;
        return true;
    },
    getNode:function(nodeX,nodeZ){
        return this._nodes[nodeX][nodeZ];
    },
    setStartNode:function(nodeX,nodeZ){
        this._startNode = this._nodes[nodeX][nodeZ];
    },
    getStartNode:function(){
        return this._startNode;
    },
    setEndNode:function(nodeX,nodeZ){
        this._endNode = this._nodes[nodeX][nodeZ];
    },
    getEndNode: function () {
        return this._endNode;
    },
    setWalkable:function(x,y,value){
        this._nodes[x][y].walkable=value;
    },
    calculateLinks:function(type){
        this._type = type;
        for (var i = 0; i < this._gridX; ++i ){
            for (var j = 0; j < this._gridZ; ++j){
                this.initNodeLink(this._nodes[i][j], this._type);
            }
        }
    },
    getType:function(){
        return this._type;
    },
    getGridX:function(){
        return this._gridX;
    },
    getGridZ:function(){
        return this._gridZ;
    },
    clone:function(){
        var gird = new iso.PathGrid( this._gridX  , this._gridZ );
        for(var i = 0; i < this._gridX; i++)
        {
            for(var j = 0; j < this._gridZ; j++)
            {
                gird.getNode(i,j).walkable = this.getNode(i,j).walkable;
            }
        }
        return gird;
    }
});

//======================================================
//======================================================

iso.Object = cc.Node.extend({
    isSort:false,
    _size:0, _spanX:0, _spanZ:0,
    _pos3D :null,
    _nodeX:1,_nodeZ:1,
    _spanPosArray:null,
    _isRotate:false,
    _boundingBox:null,
    _centerYConst:0,

    ctor:function(size , spanX, spanZ)
    {
        this._super();
        spanX = spanX || 1;
        spanZ = spanZ || 1;

        this._pos3D = {x:0,y:0,z:0};
        this._size = size;
        this._spanX = spanX;
        this._spanZ = spanZ;
        this._spanPosArray = [];
        this._boundingBox = cc.rect(0,0,0,0);
        this._centerYConst = size*spanX/2;
    },
    getSpanX:function(){
        return this._spanX;
    },
    getSpanZ:function(){
        return this._spanZ;
    },
    getCenterY:function(){
        return this.getPositionY()-this._centerYConst;
    },
    getSize:function(){
        return this._size;
    },
    boundingBox:function(){
        this._boundingBox.origin.x = this.getX();
        this._boundingBox.origin.y = this.getZ();
        if (this._isRotate) {
            this._boundingBox.size.width = this._size * (this._spanZ - 1);
            this._boundingBox.size.height = this._size * (this._spanX - 1);
        }else{
            this._boundingBox.size.height = this._size * (this._spanZ - 1);
            this._boundingBox.size.width = this._size * (this._spanX - 1);
        }
        return this._boundingBox;
    },
    sort:function(){
        this.isSort = true;
    },
    rotateX:function(value){
        this._isRotate = value;
        this.updateSpanPos();
    },
    updateScreenPos:function(){
        var pos = iso.util.isoToScreen(this._pos3D.x,this._pos3D.y,this._pos3D.z);
        this.setPosition(pos.x,pos.y);
        this.updateSpanPos();
    },
    updateSpanPos:function(){
        this._spanPosArray=[];
        var t1=0;
        var t2=0;
        if(this._isRotate){
            t1 = this._spanZ;
            t2 = this._spanX;
        }else{
            t1 = this._spanX;
            t2 = this._spanZ;
        }
        for(var i = 0 ;  i<t1 ; i++)
        {
            for(var j = 0 ; j<t2 ; j++)
            {
                var pos = {
                    x:i*this._size+this.getX(),
                    y:this.getY(),
                    z:j*this._size+this.getZ()
                } ;
                this._spanPosArray.push( pos );
            }
        }
    },
    getSpanPosArray:function(){
        return this._spanPosArray;
    },
    setX:function(value){
        this._pos3D.x = value;
        this.updateScreenPos();
        this.updateSpanPos();
    },
    getX:function(){
        return this._pos3D.x;
    },
    setY:function(value){
        this._pos3D.y = value;
    },
    getY:function(){
        return this._pos3D.y;
    },
    setZ:function(value){
        this._pos3D.z = value;
        this.updateScreenPos();
        this.updateSpanPos();
    },
    getZ:function(){
        return this._pos3D.z;
    },
    setPos3D:function(value){
        this._pos3D = value;
        this.updateScreenPos();
        this.updateSpanPos();
    },
    getPos3D:function(){
        return this._pos3D;
    },
    /**
     * 用于深度排序
     * @returns {number}
     */
    getDepth:function(){
        //return (this._pos3D.x + this._pos3D.z) * .866 - this._pos3D.y * .707;
        return -this.getCenterY();
    },

    setNodeX:function(value){
        this._nodeX = value;
        this.setX(value*this._size);
    },
    getNodeX:function(){
        return this.getX()/this._size;
    },
    setNodeZ:function(value){
        this._nodeZ = value;
        this.setZ(value*this._size);
    },
    getNodeZ:function(){
        return this.getZ()/this._size;
    },

    setScreenPos:function(x,y){
        this.setX(x);
        this.setY(y);
        this._pos3D.x = x;
        this._pos3D.y = y;
    },

    setWalkable:function(value,pathGrid){
        this.updateSpanPos();
        var len = this._spanPosArray.length;
        for(var i=0;i<len;i++){
            var v = this._spanPosArray[i];
            pathGrid.setWalkable( Math.floor(v.x/this._size) , Math.floor(v.z/this._size) , value );
        }
    },
    getWalkable:function(pathGrid){
        var flag = false;
        var len = this._spanPosArray.length;
        for(var i=0;i<len;i++){
            var v = this._spanPosArray[i];
            var nodeX = Math.floor(v.x/this._size);
            var nodeY = Math.floor(v.z/this._size);
            if(nodeX<0 || nodeX>pathGrid.getGridX()-1) return false;
            if(nodeY<0 || nodeY>pathGrid.getGridZ()-1) return false;
            flag = pathGrid.getNode(nodeX,nodeY).walkable;
            if(!flag) return false;
        }
        return true;
    },
    getRotatable:function(pathGrid){
        if (this._spanX==this._spanZ ) return true;

        this.setWalkable(true,pathGrid);
        this._isRotate = ! this._isRotate;
        this.updateSpanPos();
        var flag = this.getWalkable(pathGrid);
        //还原
        this._isRotate = ! this._isRotate;
        this.setWalkable(false,pathGrid);
        return flag;
    },
    dispose:function(){
        this._boundingBox = null;
        this._spanPosArray = null;
        this._pos3D = null;
    }
});


//=================================================
//=================================================
iso.Scene = iso.Object.extend({
    _sprites:null,
    _gridData:null,
    ctor:function( size , spanX, spanZ){
        spanX = spanX || 1;
        spanZ = spanZ || 1;

        this._super(size,spanX,spanZ);

        this._sprites = []; //所有的sprite
    },
    createGridData:function(gridX,gridZ){
        this._gridData = new iso.PathGrid(gridX,gridZ);
    },
    getGridData:function(){
        return this._gridData;
    },
    setGridData:function(value){
        this._gridData = value;
    },
    addIsoObject:function(obj,isSort){
        isSort = isSort || true;
        this.addChild(obj,0);
        this._sprites.push(obj);
        if(isSort){
            this.sortIsoObject(obj);
        }
    },
    removeIsoObject:function(obj){
        this.removeChild(obj);
        var len = this._sprites.length;
        for(var i=0;i<len;i++){
            if(this._sprites[i]==obj){
                this._sprites.splice(i,1);
                break;
            }
        }
        return obj;
    },
    sortIsoObject:function(obj){
        this.reorderChild(obj,obj.getDepth());
    },
    sortAll:function(){
        var len = this._sprites.length;
        for(var i=0;i<len;i++){
            var v = this._sprites[i];
            this.sortIsoObject(v);
            v.isSort = false;
        }
    },
    getIsoObjectByNodePos:function(nodeX,nodeZ){
        var len = this._sprites.length;
        for(var i=0;i<len;i++){
            var v = this._sprites[i];
            if(v.getNodeX==nodeX && v.getNodeZ()==nodeZ){
                return v;
            }
        }
        return null;
    },
    getIsoChildren:function(){
        return this._sprites;
    },
    clearScene:function(){
        this.removeAllChildrenWithCleanup()
        var len = this._sprites.length;
        for(var i=0;i<len;i++){
            var v = this._sprites[i];
            v.dispose();
        }
        this._sprites = [];
    }
});


//=================================================
//=================================================

/**
 * iso世界
 */
iso.World = iso.Scene.extend({
    _size:0,
    _gridX:0,
    _gridZ:0,
    _bg:null,
    _scenelayer:null,
    _scenes:[],
    ctor:function(size,gridX,gridZ){
        this._super(size,gridX,gridZ);

        this._size = size;
        this._gridX = gridX;
        this._gridZ = gridZ;

        this._scenelayer = new cc.Node();
        this.addChild(this._scenelayer,1);
    },
    getScenes:function(){
        return this._scenes;
    },
    addScene:function(isoScene){
        this._scenelayer.addChild(isoScene);
        this._scenes.push(isoScene);
        return isoScene;
    },
    panSceneLayerTo:function(x,y){
        this._scenelayer.setPosition(x,y);
    },
    getSceneOffsetX:function(){
        return this._scenelayer.getPositionX();
    },
    getSceneOffsetY:function(){
        return this._scenelayer.getPositionY();
    },
    pixelPointToGrid:function( px , py , offsetX, offsetY ){
        offsetX = offsetX || 0;
        offsetY = offsetY || 0;

        var xx = (px-this.getPositionX())/this.getScaleX() - this.getSceneOffsetX() - offsetX;
        var yy = (py-this.getPositionY())/this.getScaleY() - this.getSceneOffsetY() - offsetY;
        return iso.util.screenToIsoGrid( this._size,xx,yy);
    },
    globalPointToWorld:function(px,py){
        var xx = (px-this.getPositionX())/this.getScaleX() - this.getSceneOffsetX();
        var yy = (py-this.getPositionY())/this.getScaleY() - this.getSceneOffsetY();
        return cc.p(xx,yy);
    },
    setBg:function(bg){
        if(this._bg!=null){
            this.removeChild(this._bg,true);
        }
        this._bg = bg;
        this.addChild(this._bg,0);
    },
    clearWorld:function(){
        var len = self._scenes.length;
        for(var i=0;i<len;i++){
            var scene = self._scene[i];
            scene.clearScene();
        }
        self._scene = [];
    }
});


/**
 * 网格，辅助显示地图.
 */
iso.Grid = cc.Node.extend({
    _gridX :1,
    _gridZ :1,
    _size :0,
    _alpha :1,
    ctor:function(size , gridX , gridZ , alpha){
        this._super();
        this._gridX = gridX;
        this._gridZ = gridZ;
        this._size = size;
        this._alpha = alpha;
    },
    render:function()
    {
        this.removeAllChildren(true);
        var drawNode = new cc.DrawNode();
        var lineColor = cc.color(255,0,0,this._alpha);
        var thick = 2;
        var p = {x:0,y:0,z:0};
        for(var i=0;i<=this._gridX;i++){
            p.x = i*this._size;
            p.z = 0;
            var startPos = iso.util.isoToScreen( p.x,p.y,p.z);
            p.z = this._gridZ*this._size;
            var endPos = iso.util.isoToScreen( p.x,p.y,p.z);
            drawNode.drawSegment(cc.p(startPos.x,startPos.y), cc.p(endPos.x,endPos.y),thick, lineColor);
        }

        for(var i=0;i<=this._gridZ;i++){
            p.z = i*this._size;
            p.x = 0;
            var startPos = iso.util.isoToScreen( p.x,p.y,p.z);
            p.x = this._gridX*this._size;
            var endPos = iso.util.isoToScreen( p.x,p.y,p.z);
            drawNode.drawSegment(cc.p(startPos.x,startPos.y), cc.p(endPos.x,endPos.y),thick, lineColor);
        }
        this.addChild(drawNode);
    }
});