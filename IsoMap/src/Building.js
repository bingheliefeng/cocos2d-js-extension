/**
 * Created by zhouzhanglin on 15/8/18.
 */
var Building = iso.Object.extend({

    _container:null,

    ctor:function(spanX,spanZ){
        spanX = spanX || 1;
        spanZ = spanZ || 1;
        this._super(GRID_SIZE,spanX,spanZ);

        this._container = new cc.Node();
        this.addChild(this._container);

        this.createSkin();
    },

    createSkin:function(){
        if(this.getSpanX()==1){
            var sp = new cc.Sprite(res.Stove_png);
            sp.setPosition(-6,0);
            this._container.addChild(sp);
        }else if(this.getSpanX()==2){
            var sp = new cc.Sprite(res.Host_png);
            sp.setPosition(0,-54);
            this._container.addChild(sp);
        }
    }
});