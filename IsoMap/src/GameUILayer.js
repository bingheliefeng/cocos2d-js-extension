/**
 * Created by zhouzhanglin on 15/8/19.
 */
var GameUILayer = cc.Node.extend({

    btnBuildingOK:null,
    btnBuildingCancel:null,
    btnShopBtn:null,
    shopPanel:null,

    ctor:function(){
        this._super();

        this.btnShopBtn = ccui.Button(res.ShopBtn_png,"","",ccui.Widget.LOCAL_TEXTURE);
        this.btnShopBtn.attr({
            x:50,
            y:50,
            scaleX:0.5,
            scaleY:0.5
        });
        this.btnShopBtn.setTouchEnabled(true);
        this.btnShopBtn.setPressedActionEnabled(true);
        this.addChild(this.btnShopBtn);
        this.btnShopBtn.addClickEventListener(this.onShopClick.bind(this));
    },
    onShopClick:function(target){
        this.shopPanel = new ShopPanel();
        this.addChild(this.shopPanel);
    },
    removeShopPanel:function(){
        if(this.shopPanel!=null){
            this.shopPanel.dispose();
            this.removeChild(this.shopPanel,true);
            this.shopPanel = null;
        }
    }

});



var ShopPanel = cc.Node.extend({
    _blockLayer:null,
    ctor:function(){
        this._super();
        this._blockLayer = new BlockLayer(cc.color(0,0,0,0.1));
        this._blockLayer.runAction( cc.FadeTo(0.1,100));
        this.addChild(this._blockLayer);
    },
    dispose:function(){
        if(this._blockLayer){
            this._blockLayer.dispose();
            this._blockLayer = null;
        }
    }
});


/**
 * 挡住下面的层触摸事件
 */
var BlockLayer = cc.LayerColor.extend({
    _blockListener:null,
    ctor:function(color){
        this._super(color,cc.winSize.width,cc.winSize.height);
        this._blockListener = cc.EventListener.create({
            event:cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches:true,
            onTouchBegan:this._onThisTouchBegan.bind(this)
        });
        cc.eventManager.addListener(this._blockListener,this);
    },
    _onThisTouchBegan:function(touch,event){
        GameScene.instance.uiLayer.removeShopPanel();
        return true;
    },
    dispose:function(){
        cc.eventManager.removeListener(this._blockListener);
    }
});