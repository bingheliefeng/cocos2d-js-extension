var res = {
    HelloWorld_png : "res/HelloWorld.png",
    CloseNormal_png : "res/CloseNormal.png",
    CloseSelected_png : "res/CloseSelected.png",
    001:"res/001.png",
    002:"res/002.png",
    003:"res/003.png",
    004:"res/004.png",
    005:"res/005.png",
    006:"res/006.png",
    btn_green_m:"res/btn_green_m.png",
    btn_green_s:"res/btn_green_s.png",
    btn_orange_m:"res/btn_orange_m.png",
    btn_orange:"res/btn_orange.png",
    building_ok:"res/building_ok.png",
    building_cancle:"res/building_cancle.png",
    character:"res/character.png",
    CloseNormal:"res/CloseNormal.png",
    CloseSelected:"res/CloseSelected.png",
    email_bk:"res/email_bk.png",
    frame_add:"res/frame_add.png",
    frame_head:"res/frame_head.png",
    frame_append:"res/frame_append.png",
    frame_head_selected:"res/frame_head_selected.png",
    Info:"res/Info.png",
    Info_gray:"res/Info_gray.png",
    load_bk:"res/load_bk.jpg",
    pro_Money:"res/pro_Money.png",
    pro_Crystal:"res/pro_Crystal.png",
    pro_Money:"res/pro_Money1.png",
    pro_Crystal:"res/pro_Crystal1.png",
    RichTextBg:"res/RichTextBg.png",
    star_full_gray:"res/star_full_gray.png",
    star_full:"res/star_full.png",
    AllSprites_plist:"res/AllSprites.plist",
    AllSprites_png:"res/AllSprites.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}