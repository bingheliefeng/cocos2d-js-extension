var EventTest = cc.Layer.extend({
	flag:true,
	ctor:function(){
		this._super();
		
		function back(event)
		{
			ext.trace("back",this.flag);
		}
		var dispatcher = new ext.EventDispatcher()
		dispatcher.addEventListener(ext.Event.COMPLETE, this.callBack.bind(this));
		dispatcher.addEventListener(ext.Event.COMPLETE, back.bind(this));

		var evt = new ext.Event(ext.Event.COMPLETE);
		dispatcher.dispatchEvent(evt);
	},
	callBack :function(event)
	{
		ext.trace("callBack",this.flag);
	}
});