var UITestScene = cc.Layer.extend({

	ctor:function()
	{
		this._super();
		//test PlayerPrefs
//		PlayerPrefs.setString("name","binghe");
//		PlayerPrefs.setFloat("weight",67.5);
//		PlayerPrefs.setInt("age", 36);
//		PlayerPrefs.setBoolean("music", false);
//		PlayerPrefs.save();
//		ext.trace(PlayerPrefs.getString("name"));
//		ext.trace(PlayerPrefs.getFloat("weight"));
//		ext.trace(PlayerPrefs.getInt("age"));
//		ext.trace(PlayerPrefs.getBoolean("music"));


		//bg
		var img = new ext.Image("res/load_bk.jpg");
		img.attr({
			x:cc.winSize.width/2,
			y:cc.winSize.height/2,
			anchorX:0.5,
			anchorY:0.5
		});
		img.setScaleMode(ext.Image.SCALE_MODE_NO_BORDER, cc.winSize);
		this.addChild(img);
		//img2
		img = new ext.Image("res/star_full.png");
		img.attr({
			x:100,
			y:cc.winSize.height/4,
			anchorX:0.5,
			anchorY:0.5
		});
		img.touchEnable = true;
		img.addEventListener(ext.TouchEvent.CLICK,function(evt){
			evt.target.runAction(cc.rotateBy(0.2,20))
		});
		img.setScaleMode(ext.Image.SCALE_MODE_SHOW_ALL, cc.size(100,300));
		this.addChild(img);

		//==============================================================================
		var container = new ext.Component();
		container.attr({
			x:cc.winSize.width/2,
			y:cc.winSize.height/2
		});
		this.addChild(container);

		//按钮==============================================================================
		var normal = new cc.Sprite("res/btn_orange.png");
		var btn = new ext.Button(normal);
		btn.enableTouchScale=true;
		btn.setPosition(200,220);
		container.addChild(btn);
		btn.addEventListener(ext.TouchEvent.REAL_CLICK, function(evt){
			ext.trace("real click");
		});
		btn.addEventListener(ext.TouchEvent.CLICK, function(evt){
			ext.trace("click");
		});
		btn.addEventListener(ext.TouchEvent.CANCELED, function(evt){
			ext.trace("cancel");
		});

		//toggleBtn==============================================================================
		normal = new cc.Sprite("res/frame_add.png");
		var selected = new cc.Sprite("res/frame_append.png");
		var toggleBtn = new ext.ToggleButton(normal,selected);
		toggleBtn.enableTouchScale=true;
		toggleBtn.setPosition(-200,-220);
		container.addChild(toggleBtn);
		toggleBtn.selected = true ;


		//toggleGroup==============================================================================
		var btns = []
		for(var i=0;i<4;i++)
		{
			btn = new ext.ToggleButton(
					new cc.Sprite.create("res/building_ok.png"),
					new cc.Sprite.create("res/building_cancle.png")
			);
			btn.enableTouchScale = true;
			btn.setPosition(100*i,0);
			btns.push(btn);
		}
		var tab = new ext.ToggleGroup(btns);
		tab.addEventListener(ext.Event.CHANGE,function(evt){
			ext.trace("tab change");
		});
		tab.selectedButton = btns[2];//首先选中的按钮
		tab.setPosition(500, 100);
		this.addChild(tab);

		//滚动容器==============================================================================
//		var scroll = new ext.ScrollView(cc.size(360,360),ext.ScrollView.DIRECTION_HORIZONTAL,2);
//		var renders=[];
//		for(var i=0;i<30;i++){
//		var normal = new cc.Sprite("res/frame_head.png");
//		var btn = new ext.Button(normal);
//		btn.setTag(i);
//		btn.enableTouchScale=true;
//		btn.addEventListener(ext.TouchEvent.REAL_CLICK, function(evt){
//		ext.trace("real click tag",evt.target.getTag());
//		});
//		renders.push(btn);
//		}
//		scroll.setRenders(renders, cc.size(170,170), 10, 10);
//		container.addChild(scroll);


		//分页容器
//		var page = new ext.PageView(cc.size(760,180),ext.PageView.DIRECTION_HORIZONTAL,4);
//		for(var i=0;i<18;i++){
//		var normal = new cc.Sprite("res/frame_head.png");
//		var btn = new ext.Button(normal);
//		btn.setContentSize(cc.size(170,170));
//		btn.setTag(i);
//		btn.enableTouchScale=true;
//		btn.addEventListener(ext.TouchEvent.REAL_CLICK, function(evt){
//		ext.trace("real click tag",evt.target.getTag());
//		});
//		page.addItem(btn);
//		}
//		container.addChild(page);

//		page.addEventListener(ext.PageView.EVENT_PAGE_CHANGE, function(evt){
//		ext.trace("has next page:"+page.hasNextPage(),page.getCurrentPage()+"/"+page.getTotalPage());
//		});

		//==============================================================================
		var center = new ext.CenterPageView(cc.size(cc.winSize.width-200,200),ext.CenterPageView.DIRECTION_HORIZONTAL,-60);
		for(var i=0;i<30;i++){
			var normal = new cc.Sprite("res/frame_head.png");

			var label = new cc.LabelTTF((i+1)+"","Arial",32);
			normal.addChild(label);

			var btn = new ext.Button(normal);
			btn.setContentSize(cc.size(170,170));
			btn.setTag(i);
			center.addItem(btn); 
		}
		container.addChild(center);
		center.addEventListener(ext.CenterPageView.EVENT_PAGE_CHANGE, function(evt){
			ext.trace("has next page:"+center.hasNextPage(),center.getCurrentPage()+"/"+center.getTotalPage());
		});
		center.addEventListener(ext.CenterPageView.EVENT_SELECT, function(evt){
			ext.trace("selected:",center.getCenterRender().__tempIndex);
		});


		//==============================================================================
		//test LoadingBar
//		var loadingBar=new ext.LoadingBar("res/pro_Money.png",ext.LoadingBar.DIRECTION_RIGHT_TO_LEFT,"res/pro_Crystal.png");
		var loadingBar=new ext.LoadingBar("res/pro_Money1.png",ext.LoadingBar.DIRECTION_UP_TO_DOWN,"res/pro_Crystal1.png");
		loadingBar.setPosition(400,200);
		loadingBar.setTouchEnable(true);
		loadingBar.setPercent (0.8);
		container.addChild(loadingBar);
		
		
		//==============================================================================
		//test Slider.
		var txtSlider = new cc.LabelTTF("Value","Arial",32);
		var sliderBg = new ccui.Scale9Sprite("res/email_bk.png", cc.rect(0,0,955,203), cc.rect(20,20,955-40,203-40))
		sliderBg.setContentSize(400,70);
		var sliderThumb = new ext.Button(new cc.Sprite("res/building_cancle.png"));
		sliderThumb.enableTouchScale = true;
		var slider = new ext.Slider(sliderBg,sliderThumb,ext.Slider.DIRECTION_HORIZONTAL,0,100,5);
		slider.addEventListener(ext.Event.CHANGE, function(evt){
			txtSlider.setString(slider.value);
		})
		slider.setPosition(220,cc.winSize.height-100);
		this.addChild(slider);
		slider.addChild(txtSlider);
		slider.setValue(60);
		
		
		
		
		
		//==============================================================================
		//测试Animation序列帧动画.
		cc.spriteFrameCache.addSpriteFrames("res/AllSprites.plist");
		var frames = ext.newFrames("CoinSpin%02d.png", 1, 8);
		var animation = new cc.Animation(frames, 2/60, true);
		var animate = new cc.Animate(animation);
		var animSprite = new cc.Sprite("#CoinSpin01.png");
		container.addChild(animSprite);
		animSprite.runAction( new cc.RepeatForever(animate));
		
		
		

		//==============================================================================
		//test tweener
		var o = {x:0,y:0};
		function onStart(target,param){
			cc.log("onStart");
		}
		function onUpdate(){
			animSprite.setPosition(o.x, o.y);
		}
		function onTweenComplete(){
//			cc.log("onTweenComplete");
		}
		Tweener.addTween(o, {
			x:cc.winSize.width/3, y:cc.winSize.height/3,
			yoyo : true,
			time:1,
			onStart : onStart,
			onStartParams: {start:1},
			onUpdate : onUpdate,
			onComplete:onTweenComplete.bind(this),
			onCompleteParams:{o:o}
		});
	}


});