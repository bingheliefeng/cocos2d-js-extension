var CCUITestScene = cc.Layer.extend({

	ctor:function(){
		this._super();
	},
	onEnter:function()
	{
		this._super();

		var btn = ccui.Button("res/btn_green_m.png","","",ccui.Widget.LOCAL_TEXTURE);
		btn.attr({
			x:cc.winSize.width/2,
			y:cc.winSize.height/2,
		});
		btn.setTouchEnabled(true);
		btn.setPressedActionEnabled(true);
		btn.setTitleFontSize(40)
		btn.setTitleText("Hello world");
		this.addChild(btn);
		btn.addTouchEventListener(this.touchEvent, this);


		var page = new ccui.PageView();
		page.setContentSize(cc.winSize.width-200, 200);
		page.setPosition(100, cc.winSize.height/2);
		
		for(var i=0;i<3 ;i++){
			var layout = ccui.Layout();
			for(var j=0;j<3;j++){
				var img = ccui.Button("res/frame_add.png","","",ccui.Widget.LOCAL_TEXTURE);
				img.x += 200*(j+1);
				img.y +=100;
				layout.addChild(img);
				img.addClickEventListener(function(target){
					cc.log("click");
				});
			}
			page.addPage(layout);
		}
		this.addChild(page);
	},
	touchEvent:function(sender,type){
		switch (type) {
		case ccui.Widget.TOUCH_BEGAN:
			ext.trace("Touch Down");
			break;
		case ccui.Widget.TOUCH_MOVED:
			ext.trace("Touch Move");
			break;
		case ccui.Widget.TOUCH_ENDED:
			ext.trace("Touch Up");
			break;
		case ccui.Widget.TOUCH_CANCELED:
			ext.trace("Touch Cancelled");
			break;
		default:
			break;
		}
	}

});