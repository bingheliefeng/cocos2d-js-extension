/**
 * 数据存储
 */
var PlayerPrefs = PlayerPrefs || {}

PlayerPrefs.key = "12ABDGOdsflsf9340";//md5 key，自己设置自己的密码
PlayerPrefs._saveObj = {};//要保存的对象
PlayerPrefs._isLoaded = false;

if(cc.sys.isNative){
	PlayerPrefs._saveFile = jsb.fileUtils.getWritablePath()+"PlayerPrefs.txt";
}

PlayerPrefs.setNumber = function(key,value){
	PlayerPrefs._saveObj[key] = value;
}
PlayerPrefs.getNumber = function(key , defaultValue){
	PlayerPrefs._checkLoad();
	if(PlayerPrefs.hasKey(key)){
		return Number(PlayerPrefs._saveObj[key]);
	}
	return defaultValue;
}

PlayerPrefs.setBoolean = function(key,value){
	PlayerPrefs._saveObj[key] = value;
}
PlayerPrefs.getBoolean = function(key , defaultValue){
	PlayerPrefs._checkLoad();
	if(PlayerPrefs.hasKey(key)){
		return Boolean(PlayerPrefs._saveObj[key]);
	}
	return defaultValue;
}

PlayerPrefs.setFloat = function(key , value){
	PlayerPrefs._saveObj[key]=value;
}
PlayerPrefs.getFloat = function(key,defaultValue){
	PlayerPrefs._checkLoad();
	if(PlayerPrefs.hasKey(key)){
		return parseFloat(PlayerPrefs._saveObj[key]);
	}
	return defaultValue;
}

PlayerPrefs.setInt = function(key , value){
	PlayerPrefs._saveObj[key]=value;
}
PlayerPrefs.getInt = function(key,defaultValue){
	PlayerPrefs._checkLoad();
	if(PlayerPrefs.hasKey(key)){
		return parseInt(PlayerPrefs._saveObj[key]);
	}
	return defaultValue;
}

PlayerPrefs.setString = function(key,value){
	PlayerPrefs._checkLoad();
	PlayerPrefs._saveObj[key] = value;
}
PlayerPrefs.getString = function( key ,defaultValue){
	PlayerPrefs._checkLoad();
	if(PlayerPrefs.hasKey(key)){
		return PlayerPrefs._saveObj[key]+"";
	}
	return defaultValue;
}

PlayerPrefs.setObject = function(key,value){
	PlayerPrefs._checkLoad();
	PlayerPrefs._saveObj[key]=value;
}
PlayerPrefs.getObject = function(key,defaultValue){
	PlayerPrefs._checkLoad();
	if(PlayerPrefs.hasKey(key)){
		return PlayerPrefs._saveObj[key];
	}
	return defaultValue;
}


PlayerPrefs.hasKey = function(key){
	PlayerPrefs._checkLoad();
	return PlayerPrefs._saveObj.hasOwnProperty(key);
}

/**
 * 通过key来删除数据
 * @param key {String}
 * @return {Boolean} 是否成功
 */
PlayerPrefs.deleteKey = function(key){
	PlayerPrefs._checkLoad();
	if(PlayerPrefs.hasKey(key)){
		delete PlayerPrefs._saveObj[key];
		PlayerPrefs.save();
		return true;
	}
	return false;
}

/**
 * 删除所有的数据
 */
PlayerPrefs.deleteAll = function(){
	PlayerPrefs._checkLoad();
	PlayerPrefs._saveObj = {};
	PlayerPrefs.save();
}

/**
 * 加载保存的文件，通常只执行一次
 */
PlayerPrefs._checkLoad = function() {
	if(!cc.sys.isNative) return;
	
	if(!PlayerPrefs._isLoaded){
		PlayerPrefs._isLoaded = true;
		if(PlayerPrefs.isHaveSavedFile()){
			ext.trace(PlayerPrefs._saveFile);
			var data = jsb.fileUtils.getValueMapFromFile(PlayerPrefs._saveFile);
			if(data){
				var md5 = MD5.hex_hmac_md5(PlayerPrefs.key,data.value);
				if(md5==data.md5){
					PlayerPrefs._saveObj = JSON.parse(Base64.decode(data.value));
				}else{
					ext.trace("游戏数据被篡改");
				}
			}
		}
		else
		{
			ext.trace("未加载数据文件");
		}
	}
}

/**
 * 持久化数据
 */
PlayerPrefs.save = function(){
	var value =  Base64.encode(JSON.stringify(PlayerPrefs._saveObj));
	var md5 = MD5.hex_hmac_md5(PlayerPrefs.key,value);
	var o={md5:md5,value:value};
	if(cc.sys.isNative){
		jsb.fileUtils.writeToFile(o, PlayerPrefs._saveFile);
	}
}

/**
 * 是否保存得有数据
 * @return {Boolean}
 */
PlayerPrefs.isHaveSavedFile = function(){
	if(cc.sys.isNative){
		if(jsb.fileUtils.isFileExist( PlayerPrefs._saveFile)){
			return true;
		}
	}
	return false;
}
