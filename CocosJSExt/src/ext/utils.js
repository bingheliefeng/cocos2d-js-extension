/**
 * @author zhouzhanglin
 * 工具方法
 */

var ext= ext || {}

/**
 * 输出日志
 * <br>
 * ext.trace("h","ok")
 * </br>
 * @param 多值
 */
ext.trace = function()
{
	var temp="--TRACE-->"
	for(var i=0;i<arguments.length;i++)
	{
		temp+=arguments[i]+"	";
	}
	cc.log(temp);
}

/**
 * 子元素范围.
 * @param cc.Node
 * @return cc.rect
 */
ext.getCascadeBoundingBox = function(node)
{
	var minX = Number.MAX_VALUE;
	var minY = minX ;
	var maxX = Number.MIN_VALUE;
	var maxY = maxX;
	var children = node.getChildren();
	for (var i = 0; i < children.length; i++) {
		var child = children[i];
		if(child.isVisible()){
			var box= child.getBoundingBox();
			if( box.x<minX ) minX = box.x ;
			if( box.y<minY ) minY = box.y ;
			if( box.x+box.width>maxX ) maxX = box.x+box.width ;
			if( box.y+box.height>maxY ) maxY = box.y+box.height ;
		}
	}
	return cc.rect(minX,minY,maxX-minX,maxY-minY);
//	var nodeBox = node.getBoundingBox();
//	nodeBox.x = 0;
//	nodeBox.y = 0;
//	return cc.rectUnion(nodeBox, cc.rect(minX,minY,maxX-minX,maxY-minY));
}


/**
example 1: ext.format("%01.2f", 123.1);
returns 1: 123.10
example 2: ext.format("[%10s]", 'monkey');
returns 2: '[    monkey]'
example 3: ext.format("[%'#10s]", 'monkey');
returns 3: '[####monkey]'
example 4: ext.format("%d", 123456789012345);
returns 4: '123456789012345'
example 5: ext.format('%-03s', 'E');
returns 5: 'E00'
*/
ext.format = function(){
	var regex = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
	var a = arguments;
	var i = 0;
	var format = a[i++];

	// pad()
	var pad = function(str, len, chr, leftJustify) {
		if (!chr) {
			chr = ' ';
		}
		var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0)
		.join(chr);
		return leftJustify ? str + padding : padding + str;
	};

	// justify()
	var justify = function(value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
		var diff = minWidth - value.length;
		if (diff > 0) {
			if (leftJustify || !zeroPad) {
				value = pad(value, minWidth, customPadChar, leftJustify);
			} else {
				value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
			}
		}
		return value;
	};

	// formatBaseX()
	var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
		// Note: casts negative numbers to positive ones
		var number = value >>> 0;
		prefix = prefix && number && {
			'2': '0b',
			'8': '0',
			'16': '0x'
		}[base] || '';
		value = prefix + pad(number.toString(base), precision || 0, '0', false);
		return justify(value, prefix, leftJustify, minWidth, zeroPad);
	};

	// formatString()
	var formatString = function(value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
		if (precision != null) {
			value = value.slice(0, precision);
		}
		return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
	};

	// doFormat()
	var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) {
		var number, prefix, method, textTransform, value;

		if (substring === '%%') {
			return '%';
		}

		// parse flags
		var leftJustify = false;
		var positivePrefix = '';
		var zeroPad = false;
		var prefixBaseX = false;
		var customPadChar = ' ';
		var flagsl = flags.length;
		for (var j = 0; flags && j < flagsl; j++) {
			switch (flags.charAt(j)) {
			case ' ':
				positivePrefix = ' ';
				break;
			case '+':
				positivePrefix = '+';
				break;
			case '-':
				leftJustify = true;
				break;
			case "'":
				customPadChar = flags.charAt(j + 1);
				break;
			case '0':
				zeroPad = true;
				customPadChar = '0';
				break;
			case '#':
				prefixBaseX = true;
				break;
			}
		}

		// parameters may be null, undefined, empty-string or real valued
		// we want to ignore null, undefined and empty-string values
		if (!minWidth) {
			minWidth = 0;
		} else if (minWidth === '*') {
			minWidth = +a[i++];
		} else if (minWidth.charAt(0) == '*') {
			minWidth = +a[minWidth.slice(1, -1)];
		} else {
			minWidth = +minWidth;
		}

		// Note: undocumented perl feature:
		if (minWidth < 0) {
			minWidth = -minWidth;
			leftJustify = true;
		}

		if (!isFinite(minWidth)) {
			throw new Error('ext.format: (minimum-)width must be finite');
		}

		if (!precision) {
			precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
		} else if (precision === '*') {
			precision = +a[i++];
		} else if (precision.charAt(0) == '*') {
			precision = +a[precision.slice(1, -1)];
		} else {
			precision = +precision;
		}

		// grab value using valueIndex if required?
		value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

		switch (type) {
		case 's':
			return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
		case 'c':
			return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
		case 'b':
			return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
		case 'o':
			return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
		case 'x':
			return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
		case 'X':
			return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
			.toUpperCase();
		case 'u':
			return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
		case 'i':
		case 'd':
			number = +value || 0;
			number = Math.round(number - number % 1); // Plain Math.round doesn't just truncate
			prefix = number < 0 ? '-' : positivePrefix;
			value = prefix + pad(String(Math.abs(number)), precision, '0', false);
			return justify(value, prefix, leftJustify, minWidth, zeroPad);
		case 'e':
		case 'E':
		case 'f': // Should handle locales (as per setlocale)
		case 'F':
		case 'g':
		case 'G':
			number = +value;
			prefix = number < 0 ? '-' : positivePrefix;
			method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
			textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
			value = prefix + Math.abs(number)[method](precision);
			return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
		default:
			return substring;
		}
	};

	return format.replace(regex, doFormat);
}


/**
 *目标点是否在多边形之内(任意多边形),若p点在多边形顶点或者边上，返回值不确定，需另行判断
 *@param vertexList        多边形顶点Point数组
 *@param p                被判断点Point
 *@return     true:点在多边形内部, false:点在多边形外部
 */
ext.checkPointInPolygon = function(vertexList, p)
{
	var n = vertexList.length;
	var i = 0;
	var p1;
	var p2;
	var counter = 0;
	var xinters = 0;
	p1 = vertexList[0];
	for (i = 1; i <= n; i++)
	{
		p2 = vertexList[i % n];
		if (p.y > Math.min(p1.y, p2.y))
		{
			if (p.y <= Math.max(p1.y, p2.y))
			{
				if (p.x <= Math.max(p1.x, p2.x))
				{
					if (p1.y != p2.y)
					{
						xinters = (p.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y) + p1.x;
						if (p1.x == p2.x || p.x <= xinters)
							counter++;
					}
				}
			}
		}
		p1 = p2;
	}
	return counter % 2 != 0;
}



/**
 * 创建Sprite Frame数组
 * @param pattern {String} example:"run_%02d.png"
 * @param begin {int}
 * @param length {int}
 * @param isReversed {boolean} default=false
 * @param return {Array} SpriteFrame Array
 */
ext.newFrames = function(pattern, begin, length, isReversed){
	var frames = [];
	var step = 1;
	var last = begin + length-1 ;
	isReversed = isReversed == undefined ? false : isReversed ;
	if (isReversed){
		var temp=last;
		last = begin;
		begin = temp;
		step = -1;
	}
	for(var i=begin;i<last;i+=step){
		var frameName = ext.format(pattern, i);
		var frame = cc.spriteFrameCache.getSpriteFrame(frameName);
		if(!frame){
			cc.log( "display.newFrames() - invalid frame, name "+frameName );
			return;
		}
		frames.push(frame);
	}
	return frames;
}