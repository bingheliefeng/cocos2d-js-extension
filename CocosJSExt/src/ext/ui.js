/**
 * @author zhouzhanglin
 * UI
 */
var ext=ext || {}
/**
 * @class Component UI基类
 */
var __Component = cc.Node.extend({
	touchChildren : true, //子元素是否执行touch事件
	stopTouchEvent : true, //是否阻止触摸事件
	enableMultiTouch:false, // 默认不支持多点触摸
	_touchEnable:false, //当前元素是否执行touch事件
	_enable:true,
	_eventArray : null, //对当前类的侦听 事件
	_touchListener :null,//触摸侦听
	_touchBoundingBox:null, //手动设置触摸区域
	_touchEvt:null,
	_isRealClick:true, //是否是真实点击
	_startPoint:null, //第1次点击

	ctor:function() {
		this._super();
		this._eventArray={};
		this._touchEvt = new ext.TouchEvent();
	},
	setTouchEnable:function(value){
		if(this._touchEnable!=value){
			this._touchEnable = value;
			if(value){
				this._addTouchListeners();
			}else{
				this._removeTouchListeners();
			}
		}
	},
	getTouchEnable:function(){
		return this._touchEnable;
	},
	/**
	 * 添加触摸事件
	 */
	_addTouchListeners:function()
	{
		this._removeTouchListeners();
		this._touchListener = cc.EventListener.create({
			event:cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches:false,
			onTouchBegan:this.onTouchBegan.bind(this),
			onTouchMoved:this.onTouchMoved.bind(this),
			onTouchEnded:this.onTouchEnded.bind(this),
			onTouchCanceled:this.onTouchCanceled.bind(this)
		});
		cc.eventManager.addListener(this._touchListener, this);
	},
	_removeTouchListeners:function(){
		if(this._touchListener)
			cc.eventManager.removeListener(this._touchListener);
	},
	/**
	 * 获取范围大小，用于处理触摸
	 * @returns
	 */
	getBox:function(){
		var size = this.getContentSize();
		return cc.rect(0,0,size.width,size.height);
	},
	/**
	 * 是否吞噬触摸事件
	 * @param value
	 */
	setSwallowTouches:function(value){
		this._touchListener.swallowTouches = value;
	},
	/**
	 * 开始touch
	 * @param touch
	 * @param event
	 * @returns {Boolean}
	 */
	onTouchBegan:function(touch,event)
	{
		if(this._enable==false) return false;
		if(this.touchEnable && this._getParentTouchChildren()){
			
			var locationInNode = this.convertToNodeSpace(touch.getLocation());
			if(cc.rectContainsPoint(this.getBox(), locationInNode))
			{
				if( (touch.getID<11 && touch.getID()==0) || true ){
					if (this.stopTouchEvent==true) {
						event.stopPropagation()
					}
					this.onTouch(ext.TouchEvent.BEGAN,touch,event);
					if(this.hasEventListener(ext.TouchEvent.BEGAN)){
						this._touchEvt._type = ext.TouchEvent.BEGAN;
						this._touchEvt._touch = touch;
						this._touchEvt._event = event;
						this.dispatchEvent(this._touchEvt);
					}
					this._isRealClick = true;
					this._startPoint = touch.getLocation();
					return true;
				}
			}
		}
		return false;
	},
	
	/**
	 * 
	 * @param touch
	 * @param event
	 */
	onTouchMoved:function(touch,event)
	{
		this.onTouch(ext.TouchEvent.MOVED,touch,event);
		if(this.hasEventListener(ext.TouchEvent.MOVED)){
			this._touchEvt._type = ext.TouchEvent.MOVED;
			this._touchEvt._touch = touch;
			this._touchEvt._event = event;
			this.dispatchEvent(this._touchEvt);
		}
		var temp = cc.winSize.width>cc.winSize.height ? cc.winSize.height :cc.winSize.width;
		if(this._isRealClick && cc.pDistance(this._startPoint,touch.getLocation())>temp/640*6){
			this._isRealClick = false;
		}
	},
	/**
	 * 
	 * @param touch
	 * @param event
	 */
	onTouchEnded:function(touch,event)
	{
		this.onTouch(ext.TouchEvent.ENDED,touch,event);
		if(this.hasEventListener(ext.TouchEvent.ENDED)){
			this._touchEvt._type = ext.TouchEvent.ENDED;
			this._touchEvt._touch = touch;
			this._touchEvt._event = event;
			this.dispatchEvent(this._touchEvt);
		}
		var locationInNode = this.convertToNodeSpace(touch.getLocation());
		if(cc.rectContainsPoint(this.getBox(), locationInNode)){
			this.onTouch(ext.TouchEvent.CLICK,touch,event);
			if(this.hasEventListener(ext.TouchEvent.CLICK)){
				this._touchEvt._type = ext.TouchEvent.CLICK;
				this._touchEvt._touch = touch;
				this._touchEvt._event = event;
				this.dispatchEvent(this._touchEvt);
			}
			if(this._isRealClick){
				var temp = cc.winSize.width>cc.winSize.height ? cc.winSize.height :cc.winSize.width;
				if(cc.pDistance(this._startPoint,touch.getLocation())<temp/640*6){
					this._touchEvt._type = ext.TouchEvent.REAL_CLICK;
					this._touchEvt._touch = touch;
					this._touchEvt._event = event;
					this.dispatchEvent(this._touchEvt);
				}
			}
		}
		else
		{
			this._touchEvt._type = ext.TouchEvent.CANCELED;
			this._touchEvt._touch = touch;
			this._touchEvt._event = event;
			this.dispatchEvent(this._touchEvt);
		}
	},
	/**
	 * 
	 * @param touch
	 * @param event
	 */
	onTouchCanceled:function(touch,event)
	{
		this.onTouch(ext.TouchEvent.CANCELED,touch,event);
		if(this.hasEventListener(ext.TouchEvent.CANCELED)){
			this._touchEvt._type = ext.TouchEvent.CANCELED;
			this._touchEvt._touch = touch;
			this._touchEvt._event = event;
			this.dispatchEvent(this._touchEvt);
		}
	},
	/**
	 * 所有的触摸
	 * @param type ext.TouchEvent中的类型
	 * @param touch
	 * @param event
	 */
	onTouch:function(type,touch,event){

	},
	/**
	 * 判断父容是否设置了touchChildren
	 * @returns {Boolean}
	 */
	_getParentTouchChildren:function()
	{
		var parent = this.getParent();
		while(parent!=null)
		{
			if(parent.touchChildren==false){
				return false;
			}
			parent = parent.getParent();
		}
		return true;
	},
	getEnable:function(){
		return this._enable;
	},
	setEnable:function(value){
		this._enable = value;
	},
	removeAllListeners:function()
	{
		this.removeAllListeners()
	},
	/**
	 * 添加事件侦听
	 * @param eventType {string} 事件类型
	 * @param listener {function} 方法
	 */
	addEventListener: function(eventType,listener)
	{
		if(this._eventArray.hasOwnProperty(eventType))
		{
			this._eventArray[eventType].push(listener);
		}
		else
		{
			this._eventArray[eventType]=[listener];
		}
	},
	/**
	 * 移除事件侦听
	 * @param eventType {string} 侦听类型
	 * @param listener {function} 侦听方法
	 */
	removeEventListener: function(eventType,listener)
	{
		if(this._eventArray.hasOwnProperty(eventType))
		{
			if(!listener) delete this._eventArray[eventType];
			return;

			var listeners = this._eventArray[eventType];
			for(var i=0;i<listeners.length ; i++)
			{
				var lis = listeners[i];
				if(lis==listener)
				{
					listeners.splice(i,1);
					i--;
					if(listeners.length==0)
					{
						delete this._eventArray[eventType];
						break;
					}
				}
			}
		}
	},
	/**
	 * 判断是否有相应的事件侦听
	 * @param eventType {string} 事件类型
	 * @param listener {function} 侦听方法，可为空 
	 * @returns {Boolean} 
	 */
	hasEventListener:function(eventType,listener)
	{
		if(this._eventArray.hasOwnProperty(eventType))
		{
			if(!listener) return true;

			var listeners = this._eventArray[eventType];
			for(var i=0;i<listeners.length ; i++)
			{
				var lis = listeners[i];
				if(lis==listener)
				{
					return true;
				}
			}
		}
		return false;
	},
	/**
	 * 抛出事件
	 * @param event {ext.Event} 事件
	 */
	dispatchEvent: function(event)
	{
		if(this._eventArray.hasOwnProperty(event.getType()))
		{
			event._target= this;
			var listeners = this._eventArray[event.getType()];
			for(var i = 0 ;i<listeners.length;i++)
			{
				var lis = listeners[i];
				lis(event);
			}
		}
	},
	/**
	 * 移除所有的侦听 
	 */
	removeAllListeners:function()
	{
		this._eventArray = {}
	},
	dispose:function()
	{
		cc.eventManager.removeListener(this._touchListener);
		this.removeAllListeners();
		this._eventArray = null;
		this._touchEvt = null;
	}
});
cc.defineGetterSetter(__Component.prototype, "enable", __Component.prototype.getEnable, __Component.prototype.setEnable);
cc.defineGetterSetter(__Component.prototype, "touchEnable", __Component.prototype.getTouchEnable, __Component.prototype.setTouchEnable);
ext.Component = __Component;

//===============================================================================
/**
 * @class 按钮类
 */
var __Button = __Component.extend({
	touchScaleZoom:0.1,
	enableTouchScale:false,
	_normal: null ,
	_pressed: null,
	_disabled: null,
	_currentState:undefined,
	/**
	 * 构造函数
	 * @param normal {cc.Node}
	 * @param pressed {cc.Node}
	 * @param disabled {cc.Node}
	 */
	ctor:function(normal,pressed,disabled)
	{
		this._super();
		this.touchEnable = true;

		this._normal = normal;
		this._pressed = pressed;
		this._disabled = disabled;
		this._currentState = __Button.NORMAL;
		if(this._normal!=null){
			this.setContentSize(normal.getContentSize());
			this.addChild(this._normal);
		}
		if(this._pressed!=null){
			this.addChild(this._pressed);
			this._pressed.setVisible(false)
		}
		if(this._disabled!=null){
			this.addChild(this._disabled);
			this._disabled.setVisible(false);
		}
	},
	getBox:function(){
		if(this._touchBoundingBox!=null) return this._touchBoundingBox;
		return ext.getCascadeBoundingBox(this);
	},
	onTouch:function(type,touch,event)
	{
		if(type==ext.TouchEvent.BEGAN){
			this.showState(__Button.PRESSED);
			if (this.enableTouchScale){
				this.stopAllActions();
				var action = cc.scaleTo(0.05,1+this.touchScaleZoom);
				this.runAction(action);
			}
		}else if(type==ext.TouchEvent.ENDED||type==ext.TouchEvent.CANCELED){
			this.showState(__Button.NORMAL);
			if (this.enableTouchScale){
				this.stopAllActions();
				var action = cc.scaleTo(0.05,1);
				this.runAction(action);
			}
		}
	},
	/**
	 * 显示按钮的几种状态
	 * @param state
	 */
	showState:function( state)
	{
		this._currentState = state ;
		var children = this.getChildren();
		for (var i = 0; i < children.length; i++) {
			var child = children[i];
			child.setVisible(false);
		}
		if(state == __Button.NORMAL){
			this._normal.setVisible(true);
		}else if (state == __Button.PRESSED ){
			if (this._pressed!=null){
				this._pressed.setVisible(true);
			}else{
				this._normal.setVisible(true);
			}
		}else if(state==__Button.DISABLED){
			if(this._disabled!=null){
				this._disabled.setVisible(true);
			}else{
				this._normal.setVisible(true);
			}
		}

	},
	/**
	 * 重写setEnable方法
	 * @param value
	 */
	setEnable:function(value)
	{
		if(this._enable!=value){
			this._enable = value;
			if(value){
				this.showState(__Button.NORMAL);
			}else{
				this.showState(__Button.DISABLED);
			}
		}
	}
});
//按钮的几个状态
__Button.NORMAL   = "normal";
__Button.PRESSED  = "pressed";
__Button.DISABLED = "disabled";
ext.Button = __Button;

//===============================================================================
/**
 * Toggle按钮,抛出change(ext.Event.CHANGE)事件
 */
var __ToggleButton = __Button.extend({
	_isSelected:false,
	_selected:null,
	_normalDisabled:null,
	_selectedDisabled:null,
	ctor:function(normal,selected,normalDisabled,selectedDisabled)
	{
		this._super(normal);
		this._selected = selected;
		this._normalDisabled = normalDisabled;
		this._selectedDisabled = selectedDisabled;

		if(this._selected){
			this.addChild(this._selected);
			this._selected.setVisible(false);
		}
		if(this._normalDisabled){
			this.addChild(this._normalDisabled);
			this._normalDisabled.setVisible(false);
		}
		if(this._selectedDisabled){
			this.addChild(this._selectedDisabled);
			this._selectedDisabled.setVisible(false);
		}
	},
	onTouch:function(type,touch,event)
	{
		if(type==ext.TouchEvent.BEGAN){
			this.showState(__Button.PRESSED);
			if(!this._pressed || this.enableTouchScale){
				this.stopAllActions();
				var action = cc.scaleTo(0.05,1+this.touchScaleZoom);
				this.runAction(action);
			}
		}else if(type==ext.TouchEvent.ENDED||type==ext.TouchEvent.CANCELED){
			if (this.enableTouchScale){
				this.stopAllActions();
				var action = cc.scaleTo(0.05,1);
				this.runAction(action);
			}
		}else if(type==ext.TouchEvent.CLICK){
			this.setSelected(!this._isSelected);
			if(this.hasEventListener(ext.Event.CHANGE)){
				this.dispatchEvent(new ext.Event(ext.Event.CHANGE));
			}
		}
	},
	/**
	 * 显示按钮的几种状态
	 * @param state
	 */
	showState:function( state)
	{
		this._currentState = state ;
		if(state!=__Button.PRESSED){
			var children = this.getChildren();
			for (var i = 0; i < children.length; i++) {
				var child = children[i];
				if (child==this._normal || child ==this._pressed || child==this._disabled ||
						child==this._selected || child ==this._normalDisabled || child==this._selectedDisabled )
					child.setVisible(false);
			}
		}
		if(state == __Button.NORMAL){
			this._normal.setVisible(true);
		}else if (state == __ToggleButton.SELECTED  ){
			this._selected.setVisible(true);
		}else if (state == __ToggleButton.NORMAL_DISABLED  ){
			if (this._normalDisabled !=null){
				this._normalDisabled.setVisible(true);
			}else{
				this._normal.setVisible(true);
			}
		}else if (state == __ToggleButton.SELECTED_DISABLED   ){
			if (this._selectedDisabled!=null){
				this._selectedDisabled.setVisible(true);
			}else{
				this._selected.setVisible(true);
			}
		}
	},
	/**
	 * 重写setEnable方法
	 * @param value
	 */
	setEnable:function(value)
	{
		if(this._enable!=value){
			this._enable = value;
			if(value){
				if(this._isSelected){
					this.showState(__ToggleButton.SELECTED);
				}else{
					this.showState(__Button.NORMAL);
				}
			}else{
				if(this._isSelected){
					this.showState(__ToggleButton.SELECTED_DISABLED);
				}else{
					this.showState(__ToggleButton.NORMAL_DISABLED);
				}
			}
		}
	},
	setSelected:function(value){
		if(this._isSelected!=value){
			this._isSelected = value;
			if(value){
				if(this._enable){
					this.showState(__ToggleButton.SELECTED);
				}else{
					this.showState(__ToggleButton.SELECTED_DISABLED);
				}
			}else{
				if(this._enable){
					this.showState(__Button.NORMAL);
				}else{
					this.showState(__ToggleButton.NORMAL_DISABLED);
				}
			}
		}
	},
	isSelected:function(){
		return this._isSelected;
	},
	dispose:function(){
		this._super();
		this._selected = null;
		this._normalDisabled = null;
		this._selectedDisabled = null;
	}
});
__ToggleButton.SELECTED = "selected"
	__ToggleButton.NORMAL_DISABLED = "normalDisabled"
		__ToggleButton.SELECTED_DISABLED = "selectedDisabled"
			cc.defineGetterSetter(__ToggleButton.prototype, "selected", __ToggleButton.prototype.isSelected, __ToggleButton.prototype.setSelected);
ext.ToggleButton = __ToggleButton;

//===============================================================================
var __ToggleGroup = __Component.extend({
	_group:null, //按钮数组
	_selected:null, //当前选择的按钮
	/**
	 * 构造函数
	 * @param toggleBtnarray{Array}  ToggleButton数组
	 */
	ctor:function( toggleBtnArray ){
		this._super();
		this._group = toggleBtnArray;
		this.touchEnable = false ;
		for (var i = 0; i < toggleBtnArray.length; i++) {
			var btn = toggleBtnArray[i];
			this.addChild(btn);
			if(i==0){
				this._selected = btn;
			}
			var _this = this;
			btn.addEventListener(ext.Event.CHANGE,function(evt){
				_this._onButtonChange(evt);
			});
		}

		if(this._selected){
			this._selected.setSelected(true);
			this._selected.setEnable(true);
		}
	},
	addToggleButton:function(btn){
		this._group.push(btn);
		var _this = this;
		btn.addEventListener(ext.Event.CHANGE,function(evt){
			_this._onButtonChange(evt);
		});
		return this;
	},
	removeToggleButton:function(btn){
		for (var i = 0; i < this._group.length; i++) {
			var child = this._group[i];
			if(child==btn){
				this._group.splice(i, 1);
				btn.removeEventListener(ext.Event.CHANGE);
				this.removeChild(child,true);
				break;
			}
		}
		return this;
	},
	setSelectedButton:function(btn){
		for (var i = 0; i < this._group.length; i++) {
			var child = this._group[i];
			child.setSelected(false);
			child.setEnable(true);
		}
		btn.setSelected(true);
		this._selected = btn;
		this._selected.setEnable(false);
		if(this.hasEventListener(ext.Event.CHANGE)){
			this.dispatchEvent(new ext.Event(ext.Event.CHANGE));
		}
		return this;
	},
	getSelectedButton:function(){
		return this._selected;
	},
	_onButtonChange:function(evt){
		var btn = evt.target;
		if(btn!=this._selected){
			for (var i = 0; i < this._group.length; i++) {
				var child = this._group[i];
				if(btn==child){
					child.setSelected(true);
					this._selected = child;
					this._selected.setEnable(false);
				}else{
					child.setSelected(false);
					child.setEnable(true);
				}
			}
			if(this.hasEventListener(ext.Event.CHANGE)){
				this.dispatchEvent(new ext.Event(ext.Event.CHANGE));
			}
		}
	},
	dispose:function(){
		this._super();
		for (var i = 0; i < this._group.length; i++) {
			var child = this._group[i];
			child.dispose();
		}
		this._group = null;
	}
});
cc.defineGetterSetter(__ToggleGroup.prototype, "selectedButton", __ToggleGroup.prototype.getSelectedButton, __ToggleGroup.prototype.setSelectedButton);
ext.ToggleGroup = __ToggleGroup;

//===============================================================================
var __ScrollView=__Component.extend({
	count :1,
	direction:0,
	lockDrag:false,
	maxSpeed:60,
	speed:0.4,
	_renderSize:null,
	_row:5,
	_col:5,
	_container:null,
	_clipNode:null,
	_touchIsDown:false,
	_endX:0,
	_endY:0,
	_beganPos:null,
	_containerPos:null,
	_delta:cc.p(0,0),
	_offsetX : 0,
	_offsetY : 0,
	_scrollViewEvt:null,

	ctor:function(size,direction,count){
		this._super();
		this.touchEnable = true;
		this.setAnchorPoint(0.5, 0.5);
		this.setContentSize(size.width,size.height);
		this.count = count==undefined ? 1 : count ;
		this.direction = direction==undefined ? __ScrollView.DIRECTION_HORIZONTAL:direction;
		this._beganPos = cc.p(0,0);
		this._containerPos= cc.p(0,0);
		this._container = new cc.Node();
		this._clipNode = new cc.ClippingNode();
		this._clipNode.setStencil(this._createCliper());
		this._clipNode.setContentSize(size.width,size.height);
		if(this.direction==__ScrollView.DIRECTION_HORIZONTAL){
			this._clipNode.setPosition(0,size.height/2);
			this._row = count;
		}else if(this.direction==__ScrollView.DIRECTION_VERTICAL){
			this._clipNode.setPosition(size.width/2,size.height);
			this._col = count;
		}
		this._clipNode.addChild(this._container);
		this.addChild(this._clipNode);

		this.scheduleUpdate();
	},
	_createCliper:function()
	{
		var cliper = new cc.DrawNode();
		var size=this.getContentSize();
		if(this.direction==__ScrollView.DIRECTION_HORIZONTAL){
			cliper.drawRect(cc.p(0,-size.height/2),cc.p(size.width,size.height/2),cc.color(255, 255, 255, 255));
		}else if(this.direction==__ScrollView.DIRECTION_VERTICAL){
			cliper.drawRect(cc.p(-size.width/2,-size.height),cc.p(size.width/2,0),cc.color(255, 255, 255,255));
		}
		return cliper;
	},
	setClippingEnabled:function(value){
		this._clipNode.setClippingEnabled(value);
	},
	setRenders:function(renders,renderSize,rowGap,colGap){
		if(renders==null || renders.length==0)return;
		renderSize = renderSize || renders[0].getContentSize();
		if (renderSize.width==0 || renderSize.height==0){
			var box = renders[0].getBoundingBox()
			renderSize.width = box.width;
			renderSize.height = box.height;
		}
		rowGap = rowGap || 15;
		colGap = colGap || 5;
		this._renderSize = renderSize;
		this._container.removeAllChildren();
		this._container.setPosition(0,0);
		this._endX = 0;
		this._endY = 0;
		if (this.direction==__ScrollView.DIRECTION_HORIZONTAL){
			this._col = Math.ceil(renders.length/this._row);
			var wid = this._col*(renderSize.width+colGap)-colGap+renderSize.width;
			this._container.setContentSize( wid , renderSize.height*this._row);
			this._showRenderByH(renders,rowGap,colGap);
		}else if(this.direction==__ScrollView.DIRECTION_VERTICAL){
			this._row = Math.ceil(renders.length/this._col)
			var het =  this._row*(renderSize.height+rowGap)-rowGap+renderSize.height;
			this._container.setContentSize(renderSize.width*this._col , het );
			this._showRenderByV(renders,rowGap,colGap);
		}else{
			this._col = Math.ceil(renders.length/this._row);
			var wid = this._col*(renderSize.width+colGap)-colGap+renderSize.width;
			this._row = Math.ceil(renders.length/this._col)
			var het =  this._row*(renderSize.height+rowGap)-rowGap+renderSize.height;
			this._container.setContentSize(wid,het);
		}
	},
	onTouch:function(type,touch,event){
		if (this.lockDrag ) return ;
		var point = touch.getLocation();
		if(type==ext.TouchEvent.BEGAN){
			this._touchIsDown = true;
			this._beganPos.x = point.x;
			this._beganPos.y = point.y;
			this._containerPos.x = this._container.getPositionX();
			this._containerPos.y = this._container.getPositionY();
			this._endX = this._containerPos.x;
			this._endY = this._containerPos.y;
			this._delta.x = 0;
			this._delta.y = 0;
			this._offsetX= 0;
			this._offsetY =0;
		}else if(type==ext.TouchEvent.MOVED){
			this.touchChildren = false;
			this._offsetX = point.x-this._beganPos.x;
			this._offsetY = point.y-this._beganPos.y;

			this._endX = this._containerPos.x + this._offsetX ;
			this._endY = this._containerPos.y + this._offsetY;

			this._delta.x = touch.getDelta().x;
			this._delta.y = touch.getDelta().y;

		}else if(type==ext.TouchEvent.ENDED || type==ext.TouchEvent.CHANGE){
			this._touchIsDown =false ;
			this.touchChildren = true;

			this._offsetX = this._delta.x*10;
			this._offsetY = this._delta.y*10;


			if(this.direction==__ScrollView.DIRECTION_HORIZONTAL){
				if(Math.abs(this._offsetX)>10){
					this._endX += this._offsetX;
				}else{
					this._endX  = this._container.getPositionX();
				}
			}else if(this.direction==__ScrollView.DIRECTION_VERTICAL){
				if(Math.abs(this._offsetY)>10){
					this._endY += this._offsetY;
				}else{
					this._endY  = this._container.getPositionY();
				}
			}else if (cc.pDistance(this._containerPos,this._container.getPosition())>0){
				this._endX += this._offsetX;
				this._endY += this._offsetY;
			}else{
				this._endX  = this._container.getPositionX();
				this._endY  = this._container.getPositionY();
			}
		}
		if(type!=ext.TouchEvent.MOVED){
			if( this._endX>0 || this._container.getContentSize().width<this.getContentSize().width ){
				this._endX=0 
			}else if( this._endX<-this._container.getContentSize().width+this.getContentSize().width+this._renderSize.width ){
				this._endX = -this._container.getContentSize().width+this.getContentSize().width+this._renderSize.width
			}
			if( this._endY<0 || this._container.getContentSize().height<this.getContentSize().height ){
				this._endY=0 
			}else if( this._endY>this._container.getContentSize().height-this.getContentSize().height-this._renderSize.height ){
				this._endY = this._container.getContentSize().height-this.getContentSize().height-this._renderSize.height
			}
		}
	},
	update:function(dt){
		var sp = 0 ;
		if (this.direction==__ScrollView.DIRECTION_HORIZONTAL ){
			var speedX = (this._endX-this._container.x)*this.speed;
			if(!this._touchIsDown && this._endX+this._container.getContentSize().width>this.getContentSize().width)
				speedX *= 0.2;
			else if(!this._touchIsDown && this._endX>0 ) 
				speedX *= 0.2;
			if(speedX>this.maxSpeed)
				speedX= this.maxSpeed;
			else if(speedX <-this.maxSpeed)
				speedX= -this.maxSpeed;
			if (Math.abs(speedX)<0.1){
				speedX = 0;
				this._container.x = this._endX;
			}else{
				this._container.x += speedX;
			}
			sp = speedX;
		}
		else if(this.direction==__ScrollView.DIRECTION_VERTICAL){
			var speedY = (this._endY-this._container.y)*this.speed;
			if(!this._touchIsDown && this._endY-this._container.getContentSize().height>-this.getContentSize().height)
				speedY *= 0.2;
			else if(!this._touchIsDown && this._endY<0 ) 
				speedY *= 0.2;
			if(speedY>this.maxSpeed)
				speedY= this.maxSpeed;
			else if(speedY <-this.maxSpeed)
				speedY= -this.maxSpeed;

			if (Math.abs(speedY)<0.1){
				speedY = 0;
				this._container.y = this._endY;
			}else{
				this._container.y += speedY;
			}
			sp = speedY;
		}
		if(sp!=0){
			var children = this._container.getChildren();
			var len = children.length;
			for(var i=0;i<len;i++){
				var child = children[i];
				if(child.x+this._container.x+this._renderSize.width<0 ||
						child.x+this._container.x-this._renderSize.width>this.getContentSize().width ||
						child.y+this._container.y>this._renderSize.height||
						child.y+this._container.y+this._renderSize.height<-this.getContentSize().height)
				{
					if(child.isVisible()){
						child.setVisible(false);
						if(child.onBecameInVisibleFun){
							child.onBecameInVisibleFun();
						}
					}
				}else{
					if(!child.isVisible()){
						child.setVisible(true);
						if(child.onBecameVisibleFun){
							child.onBecameVisibleFun();
						}
					}
				}
			}
		}
	},
	_showRenderByH:function(renders,rowGap,colGap)
	{
		var tempRow = 0;
		var tempCol = 0;
		var len = renders.length;
		for (var i = 0; i < len; i++) {
			var render = renders[i];
			render.stopTouchEvent = false;
			render.setPositionX( tempCol*(this._renderSize.width+colGap) + this._renderSize.width/2 );
			render.setPositionY( -tempRow*(this._renderSize.height+rowGap) + (this._row-1)*this._renderSize.height/2 );
			this._container.addChild(render);
			tempRow = tempRow+1
			if (tempRow==this._row){
				tempRow = 0;
				tempCol = tempCol+1;
			}
		}
	},
	_showRenderByV:function(renders,rowGap,colGap)
	{
		var tempRow = 0;
		var tempCol = 0;
		var len = renders.length;
		for (var i = 0; i < len; i++) {
			var render = renders[i];
			render.stopTouchEvent = false;
			render.setPositionX( tempCol*(this._renderSize.width+colGap) - (this._col-1)*this._renderSize.width/2 )
			render.setPositionY( -tempRow*(this._renderSize.height+rowGap) - this._renderSize.height/2 )
			this._container.addChild(render);
			tempCol = tempCol+1
			if (tempCol==this._col){
				tempCol = 0;
				tempRow = tempRow+1;
			}
		}
	},
	_modifyEnd:function()
	{
		if(this._container.getChildrenCount()==0){
			this._endX=0;
			this._endY=0;
			return
		}
		if(this.direction==__ScrollView.DIRECTION_HORIZONTAL){
			this._endY=0;
		}else if(this.direction==__ScrollView.DIRECTION_VERTICAL){
			this._endX=0;
		}
	},
	dispose:function(){
		this._super();
		this.unscheduleUpdate();
		this._scrollViewEvt = null;
		this._container = null
		this._clipNode = null;
		this._renderSize= null;
		this._beganPos = null;
		this._containerPos = null ;
		this._delta = null ;
	}
});
__ScrollView.DIRECTION_BOTH         = 0;
__ScrollView.DIRECTION_HORIZONTAL   = 1;
__ScrollView.DIRECTION_VERTICAL     = 2;
ext.ScrollView = __ScrollView;

//======================================================================
var __PageView = __Component.extend({
	direction:1,
	lockDrag:false,
	speed:0.5,
	renderGap:20,
	_currentPage:0,
	_totalPage:0,
	_endPos:0,
	_beganPos:null,
	_delta:null,
	_containerPos:null,
	_enabledUpdate:false,
	_touchIsDown : false,
	_renderNum : 0 ,//render的总数量
	_perCount : 1, //一页显示几个render
	_container:null,
	_renderSize:null,

	/**
	 * 构造函数
	 * @param size 大小
	 * @param direction 方向
	 * @param perCount 一页显示几个Item
	 * @param renderGap item之间的间隔像素
	 */
	ctor:function(size,direction,perCount,renderGap){
		this._super();
		this._perCount = perCount== undefined ? 1 : perCount;
		this.renderGap = renderGap==undefined? 5:renderGap ;
		this.direction = direction;
		this._beganPos = this._delta = this._containerPos = cc.p(0,0);
		this.touchEnable = true;
		this.setAnchorPoint(0.5, 0.5);
		this.setContentSize(size.width,size.height);
		this._container = new cc.Node();
		this._clipNode = new cc.ClippingNode();
		this._clipNode.setStencil(this._createCliper());
		if(this.direction==__PageView.DIRECTION_HORIZONTAL){
			this._clipNode.setPosition(0,size.height/2);
		}else if(this.direction==__PageView.DIRECTION_VERTICAL){
			this._clipNode.setPosition(size.width/2,size.height);
		}
		this._clipNode.addChild(this._container);
		this.addChild(this._clipNode);
	},
	_createCliper:function()
	{
		var cliper = new cc.DrawNode();
		var size=this.getContentSize();
		if(this.direction==__PageView.DIRECTION_HORIZONTAL){
			cliper.drawRect(cc.p(0,-size.height/2),cc.p(size.width,size.height/2),cc.color(255, 255, 255, 255));
		}else if(this.direction==__PageView.DIRECTION_VERTICAL){
			cliper.drawRect(cc.p(-size.width/2,-size.height),cc.p(size.width/2,0),cc.color(255, 255, 255,255));
		}
		return cliper;
	},
	setClippingEnabled:function(value){
		this._clipNode.setClippingEnabled(value);
	},
	getTotalPage:function(){
		return this._totalPage+1;
	},
	getCurrentPage:function(){
		return this._currentPage+1;
	},
	hasNextPage:function(){
		return this._currentPage<this._totalPage;
	},
	hasPrevPage:function(){
		return this._currentPage>0;
	},
	nextPage:function(){
		if (this.hasNextPage()) this._currentPage++;
		var endPos = 0;
		if (this.direction==__PageView.DIRECTION_HORIZONTAL)
			endPos = -this._currentPage*this.getContentSize().width;
		else if (this.direction==__PageView.DIRECTION_VERTICAL)
			endPos = this._currentPage*this.getContentSize().height;
		if(this._endPos!=endPos){
			this._endPos = endPos;
			if(this.hasEventListener(__PageView.EVENT_PAGE_CHANGE)){
				this.dispatchEvent( new ext.Event(__PageView.EVENT_PAGE_CHANGE));
			}
			if(!this._enabledUpdate){
				this._enabledUpdate = true;
				this.scheduleUpdate();
			}
		}
	},
	prevPage:function(){
		if (this.hasPrevPage()) this._currentPage--;
		var endPos = 0;
		if (this.direction==__PageView.DIRECTION_HORIZONTAL)
			endPos = -this._currentPage*this.getContentSize().width;
		else if (this.direction==__PageView.DIRECTION_VERTICAL)
			endPos = this._currentPage*this.getContentSize().height;
		if(Math.abs(this._endPos- endPos)>1){
			this._endPos = endPos;
			if(this.hasEventListener(__PageView.EVENT_PAGE_CHANGE)){
				this.dispatchEvent( new ext.Event(__PageView.EVENT_PAGE_CHANGE));
			}
			if(!this._enabledUpdate){
				this._enabledUpdate = true;
				this.scheduleUpdate();
			}
		}
	},
	_backPage:function(){
		var endPos = 0;
		if (this.direction==__PageView.DIRECTION_HORIZONTAL)
			endPos = -this._currentPage*this.getContentSize().width;
		else if (this.direction==__PageView.DIRECTION_VERTICAL)
			endPos = this._currentPage*this.getContentSize().height;
		if(Math.abs(this._endPos- endPos)>1){
			this._endPos = endPos;
			if(this.hasEventListener(__PageView.EVENT_PAGE_CHANGE)){
				this.dispatchEvent( new ext.Event(__PageView.EVENT_PAGE_CHANGE));
			}
			if(!this._enabledUpdate){
				this._enabledUpdate = true;
				this.scheduleUpdate();
			}
		}
	},
	addItem:function(render){
		render.stopTouchEvent = false;
		if (this._renderNum>0 && this._renderNum%this._perCount==0 )
			this._totalPage++;
		if(this._renderSize==null){
			this._renderSize=render.getContentSize();
		}
		if (this.direction==__PageView.DIRECTION_HORIZONTAL ){
			var pageOffset = this.getContentSize().width*this._totalPage+this._renderSize.width/2;
			var initOffset = (this.getContentSize().width-this._renderSize.width*this._perCount-this.renderGap*(this._perCount-1) )*0.5 ;
			var px = pageOffset+initOffset+(this._renderNum%this._perCount)*(this._renderSize.width + this.renderGap) ;
			render.x = px;
		}else if (this.direction==__PageView.DIRECTION_VERTICAL ){
			var pageOffset = this.getContentSize().height*this._totalPage+this._renderSize.height/2;
			var initOffset = (this.getContentSize().height-this._renderSize.height*this._perCount-this.renderGap*(this._perCount-1) )*0.5 ;
			var py = -pageOffset-initOffset-(this._renderNum%this._perCount)*(this._renderSize.height + this.renderGap) ;
			render.y= py;
		}
		this._container.addChild(render);
		this._renderNum ++;
	},
	clearPages:function(){
		this._container.removeAllChildren();
		this._container.setPosition(0,0);
	},
	scrollToPage:function(page,animation){
		if(page<1) page=1;
		else if(page>this._totalPage) page = this._totalPage;
		this._currentPage=page;
		if( animation ){
			if( this.direction==__PageView.DIRECTION_HORIZONTAL ){
				this._endPos = -(this._currentPage-1)*this.getContentSize().width;
			}else if( this.direction==__PageView.DIRECTION_VERTICAL ){
				this._endPos = (this._currentPage-1)*this.getContentSize().height;
			}
			if(this.hasEventListener(__PageView.EVENT_PAGE_CHANGE)){
				this.dispatchEvent( new ext.Event(__PageView.EVENT_PAGE_CHANGE));
			}
			if( !this._enabledUpdate ){
				this._enabledUpdate = true;
				this.scheduleUpdate();
			}
		}else{
			if( this.direction==__PageView.DIRECTION_HORIZONTAL ){
				this._endPos = -this._currentPage*this.getContentSize().width;
				this._container.setPositionX(this._endPos);
			}else if( this.direction==__PageView.DIRECTION_VERTICAL ){
				this._endPos = this._currentPage*this.getContentSize().height;
				this._container.setPositionY(this._endPos);
			}
			if( !this._enabledUpdate ){
				this._enabledUpdate = true;
				this.scheduleUpdate();
			}
		}
	},
	scrollToRender:function(render,animation){
		if( render.getParent()==this ){
			var index = render.getLocalZOrder();
			this.scrollToPage( Math.floor(index/this._perCount),animation);
		}
	},
	onTouch:function(type,touch,event){
		if (this.lockDrag ) return ;
		var point = touch.getLocation();
		if(type==ext.TouchEvent.BEGAN){
			this._touchIsDown = true;
			this._beganPos = point;
			this._containerPos = this._container.getPosition();
			this._delta.x = 0;
			this._delta.y = 0;
			this._offsetX= 0
			this._offsetY =0
			if (this._enabledUpdate ){
				this.unscheduleUpdate();
				this._enabledUpdate = false;
			}
		}else if(type==ext.TouchEvent.MOVED){
			this.touchChildren = false;
			this._offsetX = point.x-this._beganPos.x;
			this._offsetY = point.y-this._beganPos.y;
			if ( this.direction==__PageView.DIRECTION_HORIZONTAL )
				this._endPos = this._containerPos.x +  point.x-this._beganPos.x;
			else if( this.direction==__PageView.DIRECTION_VERTICAL )
				this._endPos = this._containerPos.y +  point.y-this._beganPos.y;
			this._delta = touch.getDelta();
			if (!this._enabledUpdate ){
				this.scheduleUpdate();
				this._enabledUpdate = true;
			}
		}else if(type==ext.TouchEvent.ENDED || type==ext.TouchEvent.CANCELED){
			this._touchIsDown =false ;
			this.touchChildren = true;
			this._offsetX = point.x-this._beganPos.x;
			this._offsetY = point.y-this._beganPos.y;
			if(this.direction==__PageView.DIRECTION_HORIZONTAL){
				if(Math.abs(this._delta.x)>5 || Math.abs(this._offsetX)>this.getContentSize().width/2){			
					if (this._offsetX>0 ) this.prevPage();
					else this.nextPage()
				}else{
					this._backPage()
				}
			}else if(this.direction==__PageView.DIRECTION_VERTICAL){
				if(Math.abs(this._delta.y)>5 || Math.abs(this._offsetY)>this.getContentSize().height/2){
					if (this._offsetY<0 ) this.prevPage();
					else this.nextPage()
				}else{
					this._backPage()
				}
			}
		}
	},
	update:function(dt){
		var currentSpeed = this._touchIsDown==true ? this.speed*2 : this.speed;
		if( this.direction==__PageView.DIRECTION_HORIZONTAL ){

			if(  Math.abs(this._container.x - this._endPos)>1 ){
				this._container.setPositionX( this._container.x+(this._endPos-this._container.x)*currentSpeed*0.2);
			}else{
				this._container.setPositionX( this._endPos );
				if( ! this._touchIsDown ){
					if( this._enabledUpdate ){
						this.unscheduleUpdate();
						this._enabledUpdate = false;
					} 
					if( this.hasEventListener(__PageView.EVENT_SCROLL_OVER) ){
						this.dispatchEvent( new ext.Event(__PageView.EVENT_SCROLL_OVER));
					}
				}
			}

		}else if( this.direction==__PageView.DIRECTION_VERTICAL ){

			if(  Math.abs(this._container.y - this._endPos)>1 ){
				this._container.setPositionY( this._container.getPositionY()+(this._endPos-this._container.getPositionY())*currentSpeed*0.2);
			}else{
				this._container.setPositionY( this._endPos );
				if( ! this._touchIsDown ){  
					if( this._enabledUpdate ){
						this.unscheduleUpdate();
						this._enabledUpdate = false;
					} 
					if( this.hasEventListener(__PageView.EVENT_SCROLL_OVER) ){
						this.dispatchEvent( new ext.Event(__PageView.EVENT_SCROLL_OVER));
					}
				}
			}

		}
		var children = this._container.getChildren();
		var len = children.length;
		for(var i=0;i<len;i++){
			var child = children[i];
			if(child.x+this._container.x+this._renderSize.width*2<0 ||
					child.x+this._container.x-this._renderSize.width*2>this.getContentSize().width ||
					child.y+this._container.y>this._renderSize.height*2 ||
					child.y+this._container.y+this._renderSize.height*2<-this.getContentSize().height)
			{
				if(child.isVisible()){
					child.setVisible(false);
					if(child.onBecameInVisibleFun){
						child.onBecameInVisibleFun();
					}
				}
			}else{
				if(!child.isVisible()){
					child.setVisible(true);
					if(child.onBecameVisibleFun){
						child.onBecameVisibleFun();
					}
				}
			}
		}
	},
	dispose:function(){
		this._super();
		this._container = null ;
		this._clipNode = null;
	}
});
__PageView.EVENT_PAGE_CHANGE= "pageChange"; //页码变化事件.
__PageView.EVENT_SCROLL_OVER= "scrollOver"; //滑动结束
__PageView.DIRECTION_HORIZONTAL   = 1;
__PageView.DIRECTION_VERTICAL     = 2;
ext.PageView = __PageView;

//==========================================================
var __CenterPageView = __Component.extend({

	direction:1,
	lockDrag:false,
	speed:0.5,
	renderGap:20,
	centerScale:1.2,
	touchRenderForPage:true,//点击render切换page
	_defaultItemIndex:0,//显示哪个.
	_clickRender:false,//当前是否在点击状态
	_endPos:0,
	_beganPos:null,
	_delta:null,
	_containerPos:null,
	_enabledUpdate:false,
	_touchIsDown : false,
	_renderNum : 0 ,//render的总数量
	_perCount : 1, //一页显示几个render
	_container:null,
	_renderSize:null,
	_endX:0,
	_endY:0,
	_enabledUpdate:true,

	ctor:function(size,direction,renderGap,centerScale){
		this._super();
		this.touchEnable = true;
		this.setAnchorPoint(0.5, 0.5);
		this.setContentSize(size);
		this.direction = direction;
		this.renderGap = renderGap==undefined ? 20 :renderGap;
		this.centerScale = centerScale==undefined ? 1.2:centerScale ;

		this._beganPos = this._delta = this._containerPos = cc.p(0,0);
		this.setContentSize(size.width,size.height);
		this._container = new cc.Node();
		this._container.setPosition(size.width/2,size.height/2);
		this._clipNode = new cc.ClippingNode();
		this._clipNode.setStencil(this._createCliper());
		if(this.direction==__PageView.DIRECTION_HORIZONTAL){
			this._endX = size.width/2;
			this._endY = 0;
		}else if(this.direction==__PageView.DIRECTION_VERTICAL){
			this._endX = 0;
			this._endY = size.height/2;
		}
		this._clipNode.addChild(this._container);
		this.addChild(this._clipNode);

		this.scheduleUpdate();
	},
	_createCliper:function(){
		var cliper = new cc.DrawNode();
		var size=this.getContentSize();
		cliper.drawRect(cc.p(0,0),cc.p(size.width,size.height),cc.color(255, 255, 255, 255));
		return cliper;
	},
	addItem:function(render){
		render.touchEnable = true;
		render.stopTouchEvent = false;
		render.__tempIndex = this._container.getChildrenCount();
		if(!this._renderSize)
			this._renderSize = render.getContentSize();
		if(this.direction==__CenterPageView.DIRECTION_HORIZONTAL ){
			var px = this._container.getChildrenCount() * (this._renderSize.width + this.renderGap)
			render.x = px;
			this._container.setContentSize(px+this._renderSize.width,this._renderSize.height);
		}else if (this.direction==__CenterPageView.DIRECTION_VERTICAL ){
			var py = -this._container.getChildrenCount() * (this._renderSize.height + this.renderGap);
			render.y = py;
			this._container.setContentSize(this._renderSize.width,py+this._renderSize.height);
		}
		this._container.addChild(render);
		var _this = this;
		render.addEventListener(ext.TouchEvent.REAL_CLICK ,function(evt){
			var targetIndex = evt.target.__tempIndex;
			if (targetIndex!=_this._defaultItemIndex ){
				if(_this.touchRenderForPage){
					_this._clickRender = true;
					_this.showIndex(targetIndex,true);
				}
			}else{
				if(_this.hasEventListener(__CenterPageView.EVENT_SELECT)){
					_this.dispatchEvent(new ext.Event(__CenterPageView.EVENT_SELECT));
				}
			}
		});
	},
	showIndex:function(index , animation){
		index = index || 0;
		animation = animation==undefined ? true :animation;
		this._defaultItemIndex = index;
		if (this._defaultItemIndex<0)
			this._defaultItemIndex = 0
			else if (this._defaultItemIndex>this._container.getChildrenCount()-1 )
				this._defaultItemIndex = this._container.getChildrenCount()-1
				//页码更改.
				if(this.hasEventListener(__CenterPageView.EVENT_PAGE_CHANGE)){
					this.dispatchEvent( new ext.Event(__CenterPageView.EVENT_PAGE_CHANGE));
				}
		if (this.direction==__CenterPageView.DIRECTION_VERTICAL){
			this._endY = this._defaultItemIndex*(this._renderSize.height+this.renderGap)+this.getContentSize().height/2;
			if (!animation){
				this._container.setPositionY( this._endY );
			}else if(!this._enabledUpdate){
				this._enabledUpdate = true;
				this.scheduleUpdate()
			}
			this._scaleYRenders();
		}else{
			this._endX = -this._defaultItemIndex*(this._renderSize.width+this.renderGap)+this.getContentSize().width/2;
			if (!animation){
				this._container.setPositionX( this._endX )
			}else{
				if(!this._enabledUpdate){
					this._enabledUpdate = true;
					this.scheduleUpdate()
				}
			}
			this._scaleXRenders()
		}
	},
	scrollToRender:function(render,animation){
		this.showIndex(render.__tempIndex,animation);
	},
	update:function(dt){
		if( this.direction==__CenterPageView.DIRECTION_VERTICAL )
		{
			var speedY = (this._endY-this._container.getPositionY())*this.speed ;
			if( ! this._touchIsDown && this._endY-this._container.getContentSize().height>-this.getContentSize().height ){ 
				speedY = speedY * 0.2 ;
			}else if( ! this._touchIsDown && this._endY<0 ){ 
				speedY = speedY * 0.2 ;
			}else if( this._clickRender ){ 
				speedY = speedY * 0.2;
			}
			if( speedY>this.maxSpeed ){  
				speedY=this.maxSpeed;
			}else if( speedY <-this.maxSpeed ){  
				speedY = -this.maxSpeed;
			}
			this._container.setPositionY( this._container.getPositionY()+speedY );
			this._scaleYRenders();
			if( Math.abs(speedY)<0.1 ){
				this._container.setPositionY( this._endY );
				if( this._enabledUpdate ){
					this.unscheduleUpdate();
					this._enabledUpdate = false;
				} 
				if(this.hasEventListener(__CenterPageView.EVENT_SCROLL_OVER)){
					this.dispatchEvent(new ext.Event(__CenterPageView.EVENT_SCROLL_OVER));
				}
				this._clickRender = false;
			}
		}
		else if( this.direction==__CenterPageView.DIRECTION_HORIZONTAL ){ 
			var speedX = (this._endX-this._container.getPositionX())*this.speed 
			if(!this._touchIsDown && this._endX>-this._container.getContentSize().width-this.getContentSize().width/2 ){ 
				speedX = speedX * 0.2
			}else if( ! this._touchIsDown && this._endX<this._container.getContentSize().width/2 ){  
				speedX = speedX * 0.2
			}else if( this._clickRender ){ 
				speedX = speedX * 0.2
			}
			if(speedX>this.maxSpeed ){
				speedX= this.maxSpeed 
			}else if( speedX <-this.maxSpeed ){ 
				speedX= -this.maxSpeed 
			}
			this._container.setPositionX( this._container.getPositionX()+speedX )
			//缩放
			this._scaleXRenders()
			if( Math.abs(speedX)<0.1 ){ 
				this._container.setPositionX( this._endX )
				if( this._enabledUpdate ){
					this.unscheduleUpdate();
					this._enabledUpdate = false;
				}
				if(this.hasEventListener(__CenterPageView.EVENT_SCROLL_OVER)){
					this.dispatchEvent(new ext.Event(__CenterPageView.EVENT_SCROLL_OVER));
				}
				this._clickRender = false;
			}
		}
		//是否需要render
		var children = this._container.getChildren();
		var len = children.length;
		for (var i = 0; i < len; i++) {
			var child = children[i];
			if(  child.getPositionX()+this._container.getPositionX()+this._renderSize.width<0 ||
					child.getPositionX()+this._container.getPositionX()-this._renderSize.width>this.getContentSize().width ||
					child.getPositionY()+this._container.getPositionY()+this._renderSize.height<0 ||
					child.getPositionY()+this._container.getPositionY()-this._renderSize.height>this.getContentSize().height){
				if( child.isVisible()){ 
					child.setVisible(false);
					if( child.onBecameInVisibleFun ){ 
						child.onBecameInVisibleFun();
					}
				}
			}else{
				if( !child.isVisible()){
					child.setVisible(true);
					if( child.onBecameVisibleFun ){ 
						child.onBecameVisibleFun();
					}
				} 
			}
		}

	},
	_scaleXRenders:function(){
		if(this.centerScale>1 ){
			var children = this._container.getChildren();
			var len = children.length;
			for (var i = 0; i <len; i++) {
				var child = children[i];
				var posX =  child.getPositionX()+this._container.x-this.getContentSize().width/2;
				var sc = Math.sin( (this._renderSize.width*4-Math.abs(posX))/this._renderSize.width/4 )*this.centerScale;
				child.setScale( Math.max(this.centerScale/2,sc) );
				if (child.isVisible()){
					this._container.reorderChild(child, sc*10);
				}
			}
		}
	},
	_scaleYRenders:function(){
		if(this.centerScale>1 ){
			var children = this._container.getChildren();
			var len = children.length;
			for (var i = 0; i < len; i++) {
				var child = children[i];
				var posY =  child.getPositionY()+this._container.getPositionY()-this.getContentSize().height/2;
				var sc = Math.sin( (this._renderSize.height*4-Math.abs(posY))/this._renderSize.height/4)*this.centerScale;
				child.setScale( Math.max(this.centerScale/2,sc) );
				if (child.isVisible()){
					this._container.reorderChild(child, sc*10);
				}
			}
		}
	},
	onTouch:function(type,touch,event){
		if (this.lockDrag||this._clickRender ) return ;
		var point = touch.getLocation();
		if(type==ext.TouchEvent.BEGAN){
			this._touchIsDown = true;
			this._beganPos = point;
			this._containerPos = this._container.getPosition();
			this._delta.x = 0;
			this._delta.y = 0;
			this._offsetX= 0
			this._offsetY =0
			if (this._enabledUpdate ){
				this.unscheduleUpdate();
				this._enabledUpdate = false;
			}
		}else if(type==ext.TouchEvent.MOVED){
			this.touchChildren = false;
			this._offsetX = point.x-this._beganPos.x;
			this._offsetY = point.y-this._beganPos.y;
			this._endX = this._containerPos.x + this._offsetX ;
			this._endY = this._containerPos.y + this._offsetY;
			this._delta = touch.getDelta();
			if (!this._enabledUpdate ){
				this.scheduleUpdate();
				this._enabledUpdate = true;
			}
		}else if(type==ext.TouchEvent.ENDED || type==ext.TouchEvent.CANCELED){
			this._touchIsDown =false ;
			this.touchChildren = true;
			this._offsetX = point.x-this._beganPos.x;
			this._offsetY = point.y-this._beganPos.y;
			if(this.direction==__CenterPageView.DIRECTION_HORIZONTAL){
				if(Math.abs(this._delta.x)>10){			
					this._endX =  this._endX + this._offsetX;
				}else{
					this._endX = this._container.getPositionX();
				}
				//遍历所有的render，判断哪个与end最接近
				var minDistance = Number.MAX_VALUE;
				var minI = 0;
				var children = this._container.getChildren();
				var len = children.length;
				for (var i = 0; i < len; i++) {
					var child = children[i];
					var pos =  child.getPositionX()+this._endX;
					var dis = Math.abs(pos-this.getContentSize().width/2);
					if (dis<minDistance){
						minDistance=dis ;
						minI = child.__tempIndex;
					}
				}
				this._endX = -minI*(this._renderSize.width+this.renderGap)+this.getContentSize().width/2;
				this._defaultItemIndex = minI;
				if (Math.abs(this._endX-this._container.x)<2 ){
					this._container.setPositionX(this._endX);
				}else {
					if (!this._enabledUpdate ){
						this.scheduleUpdate();
						this._enabledUpdate = true;
					}
					if(this.hasEventListener(__CenterPageView.EVENT_PAGE_CHANGE)){
						this.dispatchEvent( new ext.Event(__CenterPageView.EVENT_PAGE_CHANGE));
					}
				}
			}else if(this.direction==__CenterPageView.DIRECTION_VERTICAL){
				if( Math.abs(this._offsetY)>10 ) this._endY =  this._endY + this._offsetY;
				else this._endY = this._container.getPositionY();
				//遍历所有的render，判断哪个与end最接近
				var minDistance = Number.MAX_VALUE;
				var minI = 0;
				var children = this._container.getChildren();
				var len = children.length;
				for (var i = 0; i < len; i++) {
					var child = children[i];
					var pos =  child.getPositionY()+this._endY;
					var dis = Math.abs(pos-this.getContentSize().height/2);
					if( dis<minDistance ){
						minDistance=dis ;
						minI = child.__tempIndex;
					}
				}
				this._endY = minI*(this._renderSize.height+this.renderGap)+this.getContentSize().height/2;
				this._defaultItemIndex = minI;
				if( Math.abs(this._endY-this._container.getPositionY())<2 ){
					this._container.setPositionY(this._endY);
				}else {
					if (!this._enabledUpdate ){
						this.scheduleUpdate();
						this._enabledUpdate = true;
					}
					if(this.hasEventListener(__CenterPageView.EVENT_PAGE_CHANGE)){
						this.dispatchEvent( new ext.Event(__CenterPageView.EVENT_PAGE_CHANGE));
					}
				}
			}
		}
	},
	getCurrentPage:function(){
		return this._defaultItemIndex+1;
	},
	getTotalPage:function(){
		return this._container.getChildrenCount();
	},
	hasNextPage:function(){
		return this._defaultItemIndex+1<this.getTotalPage();
	},
	hasPrevPage:function(){
		return this._defaultItemIndex>0;
	},
	nextPage:function(){
		if(this.hasNextPage()){
			this.showIndex(this._defaultItemIndex+1,true);
		}
	},
	prevPage:function(){
		if(this.hasPrevPage()){
			this.showIndex(this._defaultItemIndex-1,true);
		}
	},
	getRenders:function(){
		return this._container.getChildren();
	},
	getCenterRender:function(){
		var children = this._container.getChildren();
		var len = children.length;
		for (var i = 0; i < len; i++) {
			var child = children[i];
			if(child.__tempIndex==this._defaultItemIndex) return child;
		}
		return null;
	},
	dispose:function(){
		this._super();
		this._container = null; 
		this._clipNode = null; 
		this._beganPos = null; 
		this._containerPos = null; 
		this._delta = null; 
	}
});
__CenterPageView.EVENT_PAGE_CHANGE= "pageChange"; //页码变化事件.
__CenterPageView.EVENT_SCROLL_OVER= "scrollOver"; //滑动结束
__CenterPageView.EVENT_SELECT= "select"; //点击中间的一项
__CenterPageView.DIRECTION_HORIZONTAL   = 1;
__CenterPageView.DIRECTION_VERTICAL     = 2;
ext.CenterPageView = __CenterPageView;


//==========================================================
var __Image = __Component.extend({
	_sp :null,
	ctor:function( file){
		this._super();
		this.setFile(file);
	},
	setFile:function(file){
		if(this._sp){
			this.removeChild(this._sp)
		}
		if(typeof(file)=="string"){
			file = new cc.Sprite(file);
		}
		this.addChild(file);
		this._sp = file ;
	},
	setScaleMode:function(mode,size){
		if(this._sp){
			if( mode==__Image.SCALE_MODE_NO_SCALE ){
				this._sp.setScale(1)
			}else if( mode == __Image.SCALE_MODE_NO_BORDER ){
				var contentSize = this._sp.getContentSize();
				var rx =size.width/contentSize.width;
				var ry = size.height/contentSize.height;
				if( rx<ry ) this._sp.setScale(ry);
				else this._sp.setScale(rx);
			}else if( mode == __Image.SCALE_MODE_EXACT_FIT ){
				var contentSize = this._sp.getContentSize();
				var rx = size.width/contentSize.width ;
				var ry = size.height/contentSize.height;
				if( rx>ry )this._sp.setScale(ry);
				else this._sp.setScale(rx);
			}else if( mode == __Image.SCALE_MODE_SHOW_ALL ){
				var contentSize = this._sp.getContentSize();
				this._sp.setScale( size.width/contentSize.width,size.height/contentSize.height);
			}
		}
	},
	getBox:function(){
		if(this._touchBoundingBox!=null) return this._touchBoundingBox;
		return ext.getCascadeBoundingBox(this);
	},
	getSpriteFrame:function(){
		return this._sp.getSpriteFrame();
	},
	dispose:function(){
		this._super();
		this._sp = null ;
	}
});
__Image.SCALE_MODE_NO_SCALE = "noScale";
__Image.SCALE_MODE_EXACT_FIT = "exactFit"; //根据最大适应，有黑边，等比缩放，不会变形
__Image.SCALE_MODE_NO_BORDER = "noBorder"; //根据最小适应，无黑边，等比缩放，不会变形
__Image.SCALE_MODE_SHOW_ALL = "showAll"; //缩放显示，会变形
ext.Image = __Image ;


//==========================================================
/**
 * 水平和垂直进度条
 */
var __LoadingBar = __Component.extend({
	_loading:null,
	_dir :0,
	_percent:1,
	_maxSize:null,
	/**
	 * 构造函数
	 * @param imgFile loading图片
	 * @param dir 方向
	 * @param bgFile 背景图片，可为null
	 */
	ctor:function(imgFile,dir,bgFile){
		this._super();
		this._dir = dir;
		this._loading = new cc.Sprite(imgFile);
		this.addChild(this._loading,1);
		this._maxSize = this._loading.getContentSize();

		if(bgFile){
			var bg=new cc.Sprite(bgFile);
			this.addChild(bg,0);
		}

		if(this._dir==__LoadingBar.DIRECTION_LEFT_TO_RIGHT){
			this._loading.setAnchorPoint(0,0.5);
			this._loading.x = -this._maxSize.width/2;
		}else if(this._dir==__LoadingBar.DIRECTION_RIGHT_TO_LEFT){
			this._loading.setAnchorPoint(1,0.5);
			this._loading.x = this._maxSize.width/2;
		}else if(this._dir==__LoadingBar.DIRECTION_DOWN_TO_UP){
			this._loading.setAnchorPoint(0.5,0);
			this._loading.y = -this._maxSize.height/2;
		}else if(this._dir==__LoadingBar.DIRECTION_UP_TO_DOWN){
			this._loading.setAnchorPoint(0.5,1);
			this._loading.y = this._maxSize.height/2;
		}
		this.addEventListener(ext.TouchEvent.BEGAN, this._onMouseHandler.bind(this));
		this.addEventListener(ext.TouchEvent.MOVED, this._onMouseHandler.bind(this));
	},
	setPercent:function(value){
		if(this._percent!=value){
			if(value>1) value=1;
			else if(value<0) value=0;

			this._percent = value;
			if(this._dir==__LoadingBar.DIRECTION_LEFT_TO_RIGHT){
				this._loading.setTextureRect(cc.rect(0,0,this._maxSize.width*this._percent,this._maxSize.height));
			}else if(this._dir==__LoadingBar.DIRECTION_RIGHT_TO_LEFT){
				this._loading.setTextureRect(cc.rect(this._maxSize.width*(1-this._percent),0,this._maxSize.width*this._percent,this._maxSize.height));
			}else if(this._dir==__LoadingBar.DIRECTION_DOWN_TO_UP){
				this._loading.setTextureRect(cc.rect(0,this._maxSize.height*(1-this._percent),this._maxSize.width,this._maxSize.height*this._percent));
			}else if(this._dir==__LoadingBar.DIRECTION_UP_TO_DOWN){
				this._loading.setTextureRect(cc.rect(0,0,this._maxSize.width,this._maxSize.height*this._percent));
			}
			if (this.hasEventListener(ext.Event.CHANGE)){
				this.dispatchEvent(new ext.Event(ext.Event.CHANGE));
			}
		}
	},
	getPercent:function(){
		return this._percent;
	},
	getBox:function(){
		if(this._touchBoundingBox!=null) return this._touchBoundingBox;
		return ext.getCascadeBoundingBox(this);
	},
	_onMouseHandler :function(evt){
		var location = this.convertToNodeSpace(evt.touch.getLocation());
		if(this._dir==__LoadingBar.DIRECTION_LEFT_TO_RIGHT){
			this.setPercent((location.x+this._maxSize.width/2)/this._maxSize.width);
		}else if(this._dir==__LoadingBar.DIRECTION_RIGHT_TO_LEFT){
			this.setPercent((-location.x+this._maxSize.width/2)/this._maxSize.width);
		}else if(this._dir==__LoadingBar.DIRECTION_DOWN_TO_UP){
			this.setPercent((location.y+this._maxSize.height/2)/this._maxSize.height);
		}else if(this._dir==__LoadingBar.DIRECTION_UP_TO_DOWN){
			this.setPercent((-location.y+this._maxSize.height/2)/this._maxSize.height);
		}
	}
});

__LoadingBar.DIRECTION_LEFT_TO_RIGHT = 1;
__LoadingBar.DIRECTION_RIGHT_TO_LEFT = 2;
__LoadingBar.DIRECTION_UP_TO_DOWN = 3;
__LoadingBar.DIRECTION_DOWN_TO_UP = 4;
cc.defineGetterSetter(__LoadingBar.prototype, "percent", __LoadingBar.prototype.getPercent, __LoadingBar.prototype.setPercent);
ext.LoadingBar = __LoadingBar;


//================================================
/**
 * @class Slider
 */
var __Slider = __Component.extend({
	
	_dir:1,
	_background:null,
	_thumb:null,
	_min:0,
	_max:0,
	_step:0,
	_value:0,
	_thumbTouchDown:false,
	_thumbPos:null,
	
	ctor:function(background,thumb,direction,min,max,step){
		this._super();
		this._dir = direction ;
		this._min = min || 0;
		this._max = max || 100;
		this._step = step || 1;
		this._value = this._min;
		
		this._background = typeof(background)=="string"? new cc.Sprite(background) : background;
		this._thumb = typeof(thumb)=="string"? new cc.Sprite(thumb) : thumb;
		this.addChild(this._background);
		this.addChild(this._thumb);
		
		if(this._dir==__Slider.DIRECTION_HORIZONTAL){
			this._thumb.setPositionX(-this._background.getContentSize().width/2)
		}else if(this._dir==__Slider.DIRECTION_VERTICAL){
			this._thumb.setPositionY(this._background.getContentSize().height/2)
		}

		this._thumb.setTouchEnable(true)
		this._thumb.addEventListener(ext.TouchEvent.BEGAN,this._onThumbTouchHandler.bind(this));
		this._thumb.addEventListener(ext.TouchEvent.MOVED,this._onThumbTouchHandler.bind(this));
	},
	_onThumbTouchHandler:function(evt){
		switch(evt.type)
		{
		case ext.TouchEvent.BEGAN:
			this._thumbPos = this._thumb.getPosition();
			this._startPoint = evt.touch.getLocation();
			break;
		case ext.TouchEvent.MOVED:
			var prevValue = this._value;
			var box = this.getBox();
			if(this._dir==__Slider.DIRECTION_HORIZONTAL){
				
				var left = -box.width/2;
				var right = box.width/2;
				var pos = this._thumbPos.x+evt.touch.getLocation().x-this._startPoint.x;
				
				if(pos<left) pos = left;
				else if(pos>right)  pos = right;
				
				this._thumb.setPositionX(pos);
				
				var rate =(box.width/2+pos)/box.width;
				this._value = rate*(this._max-this._min)+this._min;
				this._value = Math.round(this._value/this._step)*this._step;
				//修正滑块位置
				pos = (this._value-this._min)/(this._max-this._min)*box.width-box.width/2;
				this._thumb.setPositionX(pos);
				
			}else if(this._dir==__Slider.DIRECTION_VERTICAL){
				
				var top = box.height/2;
				var bottom = -box.height/2;
				var pos = this._thumbPos.y+evt.touch.getLocation().y-this._startPoint.y;
				if(pos>top) pos =top;
				else if(pos<bottom) pos = bottom;
				var rate =(box.height/2+pos)/box.height;
				this._value = rate*(this._max-this._min)+this._min;
				this._value = Math.round(this._value/this._step)*this._step;
				//修正滑块位置
				pos = (this._value-this._min)/(this._max-this._min)*box.height-box.height/2;
				this._thumb.setPositionY(pos);
			}
			break;
		}
		//改变事件.
		if(this._value!=prevValue && this.hasEventListener(ext.Event.CHANGE)){
			this.dispatchEvent(new ext.Event(ext.Event.CHANGE));
		}
	},
	getBox:function(){
		if(this._touchBoundingBox!=null) return this._touchBoundingBox;
		return this._background.getBoundingBox();
	},
	setValue:function(value){
		var prevValue = this._value;
		if(this._value!=value){
			
			var box = this.getBox();
			if(this._dir==__Slider.DIRECTION_HORIZONTAL){

				var left = -box.width/2;
				var right = box.width/2;
				var pos = value/this._max*box.width-box.width/2;

				if(pos<left) pos = left;
				else if(pos>right)  pos = right;

				this._thumb.setPositionX(pos);

				var rate =(box.width/2+pos)/box.width;
				this._value = rate*(this._max-this._min)+this._min;
				this._value = Math.round(this._value/this._step)*this._step;
				//修正滑块位置
				pos = (this._value-this._min)/(this._max-this._min)*box.width-box.width/2;
				this._thumb.setPositionX(pos);

			}else if(this._dir==__Slider.DIRECTION_VERTICAL){
				
				var top = box.height/2;
				var bottom = -box.height/2;
				var pos = value/this._max*box.height-box.height/2;
				if(pos>top) pos =top;
				else if(pos<bottom) pos = bottom;
				var rate =(box.height/2+pos)/box.height;
				this._value = rate*(this._max-this._min)+this._min;
				this._value = Math.round(this._value/this._step)*this._step;
				//修正滑块位置
				pos = (this._value-this._min)/(this._max-this._min)*box.height-box.height/2;
				this._thumb.setPositionY(pos);
				
			}

			//改变事件.
			if(this._value!=prevValue && this.hasEventListener(ext.Event.CHANGE)){
				this.dispatchEvent(new ext.Event(ext.Event.CHANGE));
			}
		}
	},
	getMin:function(){
		return this._min;
	},
	getMax:function(){
		return this._max;
	},
	getStep:function(){
		return this._step;
	},
	getValue:function(){
		return this._value;
	},
	dispose:function(){
		this._super();
		this._thumb = null;
		this._background = null;
		this._thumbPos = null;
	}
});
__Slider.DIRECTION_HORIZONTAL = 1;
__Slider.DIRECTION_VERTICAL = 2;
cc.defineGetterSetter(__Slider.prototype, "value", __Slider.prototype.getValue, __Slider.prototype.setValue);
ext.Slider = __Slider;